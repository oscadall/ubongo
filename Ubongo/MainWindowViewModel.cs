﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Ubongo.Enums;
using Ubongo.Model;
using Ubongo.ViewModel;

namespace Ubongo
{
    public class MainWindowViewModel : PropertyChangedNotificator
    {
        private const string SavesDir = "Saves";

        public MainWindowViewModel()
        {
            CmdLevelFinished = new Command<Difficulty>(ExecuteLevelFinished);
            CmdOpenLevel = new Command<LevelState>(ExecuteOpenLevel);
            CmdGoToLevels = new Command<Difficulty>(diff => ActualCameraPosition = diff.LevelsTileCameraPosition());
            CmdGoBackToDifficultyMenu = new Command(ExecuteGoBackDifficultyMenu);
            CmdGoBackToLogin = new Command(ExecuteGoBackToLogin);
            CmdPlay = new Command(ExecutePlay);
            CmdNewPlayer = new Command(ExecuteNewPlayer);
            CmdConfirmNewPlayer = new Command(ExecuteConfirmNewPlayer);
            CmdCancelNewPlayer = new Command(ExecuteCancelNewPlayer);
            CmdDeletePlayer = new Command(ExecuteDeletePlayer);
            CmdYesDeletePlayer = new Command(ExecuteYesDeletePlayer);
            CmdNoDeletePlayer = new  Command(ExecuteNoDeletePlayer);
            CmdExitGame = new  Command(ExecuteExitGame);
            LoadPlayerSaves();

            if (Players.Count == 0)
            {
                CreateNewPlayer = true;
            }
            else
            {
                PlayerPosition = Players.First();
            }
        }

        public List<GamePosition> Players { get; set; }

        public GamePosition PlayerPosition { get; set; }

        public GameSpace GameSpace { get; set; }

        public double EdgeSize { get; } = 50;

        public List<LevelState> BeginnerLevels { get; set; }
        public List<LevelState> MiddleLevels { get; set; }
        public List<LevelState> ExpertLevels { get; set; }
        public List<LevelState> DevilLevels { get; set; }

        public bool IsTaskFinished { get; set; }

        public bool ShowTask { get; set; }

        public int OpenedTaskNumber { get; set; }

        public ICommand CmdLevelFinished { get; }
        public ICommand CmdOpenLevel { get; }
        public ICommand CmdGoToLevels { get; }
        public ICommand CmdGoBackToDifficultyMenu { get; }
        public ICommand CmdGoBackToLogin { get; }
        public ICommand CmdPlay { get; }
        public ICommand CmdNewPlayer { get; }
        public ICommand CmdConfirmNewPlayer { get; }
        public ICommand CmdCancelNewPlayer { get; }
        public ICommand CmdDeletePlayer { get; }
        public ICommand CmdYesDeletePlayer { get; }
        public ICommand CmdNoDeletePlayer { get; }
        public ICommand CmdExitGame { get; }

        public string NewPlayerName { get; set; }
        public bool CreateNewPlayer { get; set; }
        public bool DeletePlayer { get; set; }
        public string NewPlayerError { get; set; }

        public GameCameraPositions ActualCameraPosition { get; set; }

        public void Init()
        {
            ActualCameraPosition = GameCameraPositions.Login;
        }

        private async void ExecutePlay()
        {
            SetNewLevelStates(PlayerPosition);
            await Task.Delay(500);
            ActualCameraPosition = GameCameraPositions.MenuDifficulty;
        }


        private async void ExecuteLevelFinished(Difficulty difficulty)
        {
            PlayerPosition.SetAsSolved(OpenedTaskNumber,difficulty);
            PlayerPosition.Save(SavesDir);

            IsTaskFinished = true;
            await Task.Delay(2000);
            ShowTask = false;
            ActualCameraPosition = difficulty.LevelsTileCameraPosition();
        }

        private void ExecuteOpenLevel(LevelState task)
        {
            var newTask = GetLevel(task);
            if (GameSpace?.Equals(newTask)?? false)
                GameSpace = null;

            OpenedTaskNumber = task.Level;
            GameSpace = newTask;
            IsTaskFinished = false;
            ShowTask = true;
            ActualCameraPosition = task.Difficulty.GameTileCameraPosition();
        }

        private GameSpace GetLevel(LevelState level)
        {
            var index = level.Level - 1;
            switch (level.Difficulty)
            {
                case Difficulty.Beginner:
                    return BaseTasks.GetTask(index);
                case Difficulty.Middle:
                    return MiddleTasks.GetTask(index);
                case Difficulty.Expert:
                    return ExpertTasks.GetTask(index);
                case Difficulty.Devil:
                    return DevilTasks.GetTask(index);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void ExecuteGoBackDifficultyMenu()
        {
            ActualCameraPosition = GameCameraPositions.MenuDifficulty;
        }

        private void ExecuteGoBackToLogin()
        {
            ActualCameraPosition = GameCameraPositions.Login;
        }

        private void ExecuteNewPlayer()
        {
            CreateNewPlayer = true;
        }

        private void ExecuteConfirmNewPlayer()
        {
            if (string.IsNullOrWhiteSpace(NewPlayerName))
            {
                NewPlayerError = "Player name can not be empty";
                return;
            }

            if (Players.FirstOrDefault(gp => gp.Player == NewPlayerName.Trim()) != null)
            {
                NewPlayerError = "This name already exists";
                return;
            }

            var player = new GamePosition(NewPlayerName.Trim());
            Players.Add(player);
            PlayerPosition = player;
            player.Save(SavesDir);

            CreateNewPlayer = false;
        }

        private void ExecuteCancelNewPlayer()
        {
            CreateNewPlayer = false;
        }

        private void ExecuteDeletePlayer()
        {
            DeletePlayer = true;
        }

        private void ExecuteYesDeletePlayer()
        {
            PlayerPosition.DeleteFrom(SavesDir);
            Players.Remove(PlayerPosition);
            PlayerPosition = null;
            OnPropertyChanged(nameof(Players));
            DeletePlayer = false;
        }

        private void ExecuteNoDeletePlayer()
        {
            DeletePlayer = false;
        }

        private void SetNewLevelStates(GamePosition gamePosition)
        {
            if(gamePosition == null)
                return;

            BeginnerLevels?.ForEach(l => l.Dispose());
            MiddleLevels?.ForEach(l => l.Dispose());
            ExpertLevels?.ForEach(l => l.Dispose());
            DevilLevels?.ForEach(l => l.Dispose());
            var b = new List<LevelState>();
            var m = new List<LevelState>();
            var e = new List<LevelState>();
            var d = new List<LevelState>();
            for (int i = 1; i <= 48; i++)
            {
                b.Add(new LevelState(i, Difficulty.Beginner, gamePosition));
                m.Add(new LevelState(i, Difficulty.Middle, gamePosition));
                e.Add(new LevelState(i, Difficulty.Expert, gamePosition));
            }
            for (int i = 1; i <= 6; i++)
            {
                d.Add(new LevelState(i, Difficulty.Devil, gamePosition));
            }

            BeginnerLevels = b;
            MiddleLevels = m;
            ExpertLevels = e;
            DevilLevels = d;
        }

        private void LoadPlayerSaves()
        {
            try
            {
                if (!Directory.Exists(SavesDir))
                    Directory.CreateDirectory(SavesDir);

                var files = Directory.GetFiles(SavesDir).Select(File.ReadAllBytes);
                Players = files.Select(gp => gp.ConvertToGamePosition()).ToList();
            }
            catch (Exception)
            {
               Players = new List<GamePosition>();
            }
        }

        private void ExecuteExitGame()
        {
            System.Windows.Application.Current.Shutdown();
        }
    }
}
