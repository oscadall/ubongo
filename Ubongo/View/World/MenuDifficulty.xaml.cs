﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ubongo.View.World
{
    /// <summary>
    /// Interaction logic for MenuDifficulty.xaml
    /// </summary>
    public partial class MenuDifficulty : WorldTile
    {
        public MenuDifficulty()
        {
            InitializeComponent();
        }

        public override void OnWindowSizeChanged(double width, double height)
        {
            Viewbox.Width = Viewbox.Height = Math.Max(width, height);
        }

        private void UIElement_OnPreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }
    }
}
