﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Ubongo.Enums;

namespace Ubongo.View.World
{
    /// <summary>
    /// Interaction logic for GameTile.xaml
    /// </summary>
    public partial class GameTile : WorldTile
    {
        public GameTile()
        {
            InitializeComponent();
        }

        public static DependencyProperty DifficultyProperty = DependencyProperty.Register(nameof(Difficulty), typeof(Difficulty), typeof(GameTile), new PropertyMetadata(null));

        public Difficulty Difficulty
        {
            get { return (Difficulty)GetValue(DifficultyProperty); }
            set { SetValue(DifficultyProperty, value); }
        }

        public override void OnWindowSizeChanged(double width, double height)
        {
            Viewbox.Width = Viewbox.Height = Math.Max(width, height);
        }

        protected override void OnTableImageSourceChanged()
        {
            GameSpaceControl.Table = TableImageSource;
        }
    }
}
