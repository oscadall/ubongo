﻿using System;
using System.Collections;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;


namespace Ubongo.View.World
{
    /// <summary>
    /// Interaction logic for LevelsTile.xaml
    /// </summary>
    public partial class LevelsTile : WorldTile
    {
        public LevelsTile()
        {
            InitializeComponent();
            ImageBrush.TileMode = TileMode.Tile;
            ImageBrush.ViewportUnits = BrushMappingMode.Absolute;
        }

        protected override void OnBackgroundImageSourceChanged()
        {
            ImageBrush.Viewport = new Rect(0, 0, BackgroundImageSource.Width, BackgroundImageSource.Height);
        }

        public static DependencyProperty TasksProperty = DependencyProperty.Register(nameof(Tasks), typeof(IEnumerable), typeof(LevelsTile), new PropertyMetadata(null));

        public static DependencyProperty OpenLevelCommandProperty = DependencyProperty.Register(nameof(OpenLevelCommand), typeof(ICommand), typeof(LevelsTile), new PropertyMetadata(null));

        public IEnumerable Tasks
        {
            get { return (IEnumerable)GetValue(TasksProperty); }
            set { SetValue(TasksProperty, value); }
        }

        public ICommand OpenLevelCommand
        {
            get { return (ICommand)GetValue(OpenLevelCommandProperty); }
            set { SetValue(OpenLevelCommandProperty, value); }
        }

        public override void OnWindowSizeChanged(double width, double height)
        {
            Viewbox.Width = Viewbox.Height = Math.Max(width, height);
        }
    }
}
