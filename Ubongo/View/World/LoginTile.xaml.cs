﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;

namespace Ubongo.View.World
{
    /// <summary>
    /// Interaction logic for LoginTile.xaml
    /// </summary>
    public partial class LoginTile : WorldTile
    {
        private MainWindowViewModel ViewModel => DataContext as MainWindowViewModel;
        private FrameworkElement NewPlayerCreation => (FrameworkElement) Resources["NewPlayerCreation"];
        private FrameworkElement UserSelection => (FrameworkElement) Resources["UserSelection"];
        private FrameworkElement DeleteQuestion => (FrameworkElement)Resources["DeleteQuestion"];

        public LoginTile()
        {
            InitializeComponent();
            DataContextChanged += LoginTile_DataContextChanged;
            Loaded += LoginTile_Loaded;
        }

        public override void OnWindowSizeChanged(double width, double height)
        {
            Viewbox.Width = Viewbox.Height = Math.Max(width, height);
        }

        private void LoginTile_Loaded(object sender, RoutedEventArgs e)
        {
            var content = ViewModel?.CreateNewPlayer ?? false
                ? NewPlayerCreation
                : UserSelection;
            SetContent(content);
        }

        private void LoginTile_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (DataContext is MainWindowViewModel wm)
            {
                wm.PropertyChanged += ViewModel_PropertyChanged;
                DataContextChanged -= LoginTile_DataContextChanged;
            }
        }

        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(MainWindowViewModel.CreateNewPlayer))
            {
                var content = ViewModel.CreateNewPlayer
                    ? NewPlayerCreation
                    : UserSelection;
                SetContent(content);
            }

            if (e.PropertyName == nameof(MainWindowViewModel.DeletePlayer))
            {
                var content = ViewModel.DeletePlayer
                    ? DeleteQuestion
                    : UserSelection;
                SetContent(content);
            }
        }

        private async void SetContent(FrameworkElement content)
        {
            var animationMiliseconds = 500;
            var decRat = 0.9;

            if (ContentControl.HasContent)
            {
                var firstStoryboard = new Storyboard();
                var animation = new DoubleAnimation
                {
                    Duration = new Duration(TimeSpan.FromMilliseconds(animationMiliseconds)),
                    From = 1,
                    To = 0,
                    DecelerationRatio = decRat
                };
                Storyboard.SetTargetProperty(animation, new PropertyPath("Opacity"));
                firstStoryboard.Children.Add(animation);
                firstStoryboard.Begin((FrameworkElement)ContentControl.Content);
                await Task.Delay(animationMiliseconds);
            }

            ContentControl.Content = content;

            var secondStoryboard = new Storyboard();
            var animation2 = new DoubleAnimation
            {
                Duration = new Duration(TimeSpan.FromMilliseconds(animationMiliseconds)),
                From = 0,
                To = 1,
                AccelerationRatio = decRat
            };
            Storyboard.SetTargetProperty(animation2, new PropertyPath("Opacity"));
            secondStoryboard.Children.Add(animation2);
            secondStoryboard.Begin(content);
        }
    }
}
