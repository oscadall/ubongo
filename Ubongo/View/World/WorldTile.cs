﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Ubongo.View.World
{
    public class WorldTile : UserControl
    {
        public static int TileHeight = 1920;
        public static int TileWidth = 1920;
        public static double TileSize = 1920;

        protected WorldTile()
        {
            Width = TileWidth;
            Height = TileHeight;
        }

        public static DependencyProperty TableImageSourceProperty = DependencyProperty.Register(nameof(TableImageSource), typeof(ImageSource), typeof(WorldTile), new PropertyMetadata(null, TableSourceChangedCallback));

        public static DependencyProperty BackgroundImageSourceProperty = DependencyProperty.Register(nameof(BackgroundImageSource), typeof(ImageSource), typeof(WorldTile), new PropertyMetadata(null, PropertyChangedCallback));

        public ImageSource TableImageSource
        {
            get { return (ImageSource) GetValue(TableImageSourceProperty);}
            set { SetValue(TableImageSourceProperty, value);}
        }

        public ImageSource BackgroundImageSource
        {
            get { return (ImageSource)GetValue(BackgroundImageSourceProperty); }
            set { SetValue(BackgroundImageSourceProperty, value); }

        }

        protected virtual void OnBackgroundImageSourceChanged()
        {

        }

        protected virtual void OnTableImageSourceChanged()
        {

        }

        public virtual void OnWindowSizeChanged(double width, double height)
        {

        }

        private static void PropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((WorldTile)d).OnBackgroundImageSourceChanged();
        }

        private static void TableSourceChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((WorldTile)d).OnTableImageSourceChanged();
        }
    }
}
