﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Ubongo.View.World
{
    /// <summary>
    /// Interaction logic for WorldControl.xaml
    /// </summary>
    public partial class WorldControl : UserControl
    {
        public WorldControl()
        {
            InitializeComponent();
            ImageBrush.ImageSource = new BitmapImage(new Uri("pack://application:,,,/../../Resources/grass_tile.jpg", UriKind.RelativeOrAbsolute));
            ImageBrush.TileMode = TileMode.Tile;
            ImageBrush.ViewportUnits = BrushMappingMode.Absolute;
            ImageBrush.Viewport = new Rect(0, 0, ImageBrush.ImageSource.Width, ImageBrush.ImageSource.Height);
        }

        public void OnWindowSizeChanged(double width, double height)
        {
            LevelsTileBeginner.OnWindowSizeChanged(width,height);
            LevelsTileMiddle.OnWindowSizeChanged(width,height);
            LevelsTileExpert.OnWindowSizeChanged(width,height);
            LevelsTileDevil.OnWindowSizeChanged(width,height);
            GameTileBeginner.OnWindowSizeChanged(width,height);
            GameTileMiddle.OnWindowSizeChanged(width,height);
            GameTileExpert.OnWindowSizeChanged(width,height);
            GameTileDevil.OnWindowSizeChanged(width,height);
            LoginTile.OnWindowSizeChanged(width,height);
            Difficulty.OnWindowSizeChanged(width,height);
        }
    }
}
