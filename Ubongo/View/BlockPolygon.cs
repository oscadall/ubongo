﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Ubongo.Animations;
using Ubongo.Model;

namespace Ubongo.View
{
    public class BlockPolygon : Canvas
    {
        private const double BorderThicknies = 1;
        private readonly RotateTransform rotateTransform = new RotateTransform();
        private readonly ScaleTransform scaleTransform = new ScaleTransform();
        private static SolidColorBrush BorderBrush = new SolidColorBrush(Color.FromArgb(170,105,105,105));
        private bool rotationFlag = false;
        private bool moveFlag = false;

        private static Dictionary<string, Brush> polygonBrushes = new Dictionary<string, Brush>
        {
            { Blocks.A.Name , Brushes.DarkMagenta },
            { Blocks.B.Name , Brushes.DarkCyan },
            { Blocks.C.Name , Brushes.Gold },
            { Blocks.D.Name , Brushes.Green },
            { Blocks.E.Name , Brushes.OrangeRed },
            { Blocks.F.Name , Brushes.Fuchsia},
            { Blocks.G.Name , Brushes.Lime },
            { Blocks.H.Name , Brushes.SaddleBrown },
            { Blocks.I.Name , Brushes.Red},
            { Blocks.J.Name , Brushes.Yellow },
            { Blocks.K.Name , Brushes.DodgerBlue },
            { Blocks.L.Name , Brushes.Blue },
            { Blocks.M.Name , Brushes.Brown},
            { Blocks.N.Name , Brushes.LightSeaGreen },
            { Blocks.O.Name , Brushes.Orange},
            { Blocks.P.Name , Brushes.Tomato },
            { Blocks.Q.Name , Brushes.Crimson },
            { Blocks.R.Name , Brushes.DarkSlateGray },
            { Blocks.S.Name , Brushes.BurlyWood },
        };

        public BlockPolygon(Block block, double edgeSize)
        {
            Block = block;
            EdgeSize = edgeSize;
            Brush = polygonBrushes[block.Name];
            CreateCells();
            Width = EdgeSize * Block.Width;
            Height = EdgeSize * Block.Height;
            RenderTransformOrigin = new Point(0.5, 0.5);

            var  tg = new TransformGroup();
            tg.Children.Add(rotateTransform);
            tg.Children.Add(scaleTransform);
            RenderTransform = tg;
            Rotation = System.Windows.Media.Imaging.Rotation.Rotate0;
        }

        public Block Block { get; private set; }

        public double EdgeSize { get; }

        public Brush Brush { get; set; }

        public Rotation? Rotation { get; private set; }

        public bool InvertedX { get; private set; }

        public bool InvertedY { get; private set; }

        public Point GetTransformedPosition(Point position)
        {
            return rotateTransform.Transform(position);
        }

        private void CreateCells()
        {
            for (int i = 0; i < Block.Width; i++)
            {
                for (int j = 0; j < Block.Height; j++)
                {
                    if (!Block[i, j])
                        continue;

                    var cell = new Border
                    {
                        BorderBrush = Brushes.Black,
                        BorderThickness = new Thickness(0.5),
                        Background = Brush,
                        Width = EdgeSize,
                        Height = EdgeSize,
                    };
                    Children.Add(cell);
                    SetLeft(cell, i * EdgeSize);
                    SetTop(cell, j * EdgeSize);


                    if (LeftEdge(i, j))
                    {
                        var edge = new Rectangle
                        {
                            Fill = BorderBrush,
                            Width = BorderThicknies,
                            Height = EdgeSize
                        };
                        Children.Add(edge);
                        SetLeft(edge, i * EdgeSize);
                        SetTop(edge, j * EdgeSize);
                    }

                    if (RightEdge(i, j))
                    {
                        var edge = new Rectangle
                        {
                            Fill = BorderBrush,
                            Width = BorderThicknies,
                            Height = EdgeSize
                        };
                        Children.Add(edge);
                        SetLeft(edge, (i + 1) * EdgeSize - BorderThicknies);
                        SetTop(edge, j * EdgeSize);
                    }

                    if (UpperEdge(i, j))
                    {
                        var edge = new Rectangle
                        {
                            Fill = BorderBrush,
                            Width = EdgeSize,
                            Height = BorderThicknies
                        };
                        Children.Add(edge);
                        SetLeft(edge, i * EdgeSize);
                        SetTop(edge, j * EdgeSize);
                    }

                    if (BottomEdge(i, j))
                    {
                        var edge = new Rectangle
                        {
                            Fill = BorderBrush,
                            Width = EdgeSize,
                            Height = BorderThicknies
                        };
                        Children.Add(edge);
                        SetLeft(edge, i * EdgeSize);
                        SetTop(edge, (j + 1) * EdgeSize - BorderThicknies);
                    }
                }
            }
        }

        private bool LeftEdge(int x, int y)
        {
            return x == 0 || !Block[x - 1, y];
        }

        private bool RightEdge(int x, int y)
        {
            return x == Block.Width - 1 || !Block[x + 1, y];
        }

        private bool UpperEdge(int x, int y)
        {
            return y == 0 || !Block[x, y - 1];
        }

        private bool BottomEdge(int x, int y)
        {
            return y == Block.Height - 1 || !Block[x, y + 1];
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            if(rotationFlag || moveFlag)
                return;

            moveFlag = true;

            SetZIndex(this, GetZIndex(this) + 20);

            base.OnMouseLeftButtonDown(e);
            if (Rotation == null)
            {
                return;
            }

            CaptureMouse();

            if (e.ClickCount == 2)
            {
               Invert();
            }
        }

        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            if(!moveFlag)
                return;

            base.OnMouseLeftButtonUp(e);
            ReleaseMouseCapture();

            moveFlag = false;
        }

        protected override void OnMouseRightButtonDown(MouseButtonEventArgs e)
        {
            if (rotationFlag || moveFlag)
                return;
            rotationFlag = true;

            SetZIndex(this, Canvas.GetZIndex(this) + 20);

            base.OnMouseRightButtonDown(e);

            if (Rotation == null)
            {
                return;
            }
            Rotate();
        }


        private async void Rotate()
        {
            Rotation = null;

            var rotationDelay = 200;
            var storyboard = new Storyboard();
            var newAngle = InvertedX == InvertedY ? rotateTransform.Angle - 90 : rotateTransform.Angle + 90;

            storyboard.AddRotationAnimation(rotationDelay, rotateTransform.Angle, (int)newAngle);
            storyboard.Begin(this);
            await Task.Delay(rotationDelay);
            Block.Rotate();

            var angle360 = 360 + rotateTransform.Angle % 360;
            Rotation = (Rotation) (int) ((angle360 / 90)  % 4);

            rotationFlag = false;
        }

        private async void Invert()
        {
            Block.Invert();
            if (Rotation == System.Windows.Media.Imaging.Rotation.Rotate0 ||
                Rotation == System.Windows.Media.Imaging.Rotation.Rotate180)
                InvertedX = !InvertedX;
            else
            {
                InvertedY = !InvertedY;
            }

            var storyboard = new Storyboard();
            var rotationDelay = 200;
            var animation = new DoubleAnimation
            {
                Duration = new Duration(TimeSpan.FromMilliseconds(rotationDelay)),
                From = scaleTransform.ScaleX,
                To = scaleTransform.ScaleX * -1,
                DecelerationRatio = 0.9
            };

            Storyboard.SetTargetProperty(animation, new PropertyPath("RenderTransform.Children[1].ScaleX"));
            storyboard.Children.Add(animation);
            storyboard.Begin(this);
            await Task.Delay(rotationDelay);
        }
    }
}
