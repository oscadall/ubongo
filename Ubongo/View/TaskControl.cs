﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Ubongo.Model;

namespace Ubongo.View
{
    public class TaskControl : Canvas
    {

        private static ImageSource tile;

        static TaskControl()
        {
            tile = new BitmapImage(new Uri("pack://application:,,,/Blocks;component/Resources/ctverec_zadani.png"));
        }

        public TaskControl()
        {

        }

        public TaskControl(GameSpace gameSpace, double edgeSize)
        {
            GameSpace = gameSpace;
            EdgeSize = edgeSize;
            UpdateSize();
        }

        public static readonly DependencyProperty GameSpaceProperty = DependencyProperty.Register(nameof(GameSpace),
                typeof(GameSpace),
                typeof(TaskControl),
                new FrameworkPropertyMetadata(null, (s, a) => { ((TaskControl)s).UpdateSize();}));
       
        public GameSpace GameSpace
        {
            get => (GameSpace)GetValue(GameSpaceProperty);
            set => SetValue(GameSpaceProperty, value);
        }

        public static readonly DependencyProperty EdgeSizeProperty = DependencyProperty.Register(nameof(EdgeSize), 
            typeof(double), 
            typeof(TaskControl), 
            new FrameworkPropertyMetadata(50.0, (s, a) => { ((TaskControl)s).UpdateSize(); }));

        public double EdgeSize
        {
            get => (double)GetValue(EdgeSizeProperty);
            set => SetValue(EdgeSizeProperty, value);
        }

        public void UpdateSize()
        {
            Height = EdgeSize * GameSpace?.Height ?? 0;
            Width = EdgeSize * GameSpace?.Width ?? 0;
        }


        //public Point GetAlignment(Point position, out int x, out int y)
        //{
        //    x = (int) Math.Round(position.X / EdgeSize);
        //    y = (int) Math.Round(position.Y / EdgeSize);

        //    var alignX = x * EdgeSize;
        //    var alignY = y * EdgeSize;

        //    return new Point(alignX,alignY);
        //}

        

        protected override void OnRender(DrawingContext dc)
        {
            base.OnRender(dc);

            const double cellMargin = 0;
            for (int i = 0; i < GameSpace.Width; i++)
            {
                for (int j = 0; j < GameSpace.Height; j++)
                {
                    if(GameSpace[i, j])
                        //dc.DrawRectangle(Brushes.Gray,
                        //null,
                        //new Rect(new Point(i * EdgeSize + cellMargin, j * EdgeSize + cellMargin),
                        //    new Size(EdgeSize - 2 * cellMargin, EdgeSize - 2 * cellMargin)));
                    dc.DrawImage(tile, new Rect(new Point(i * EdgeSize + cellMargin, j * EdgeSize + cellMargin),
                        new Size(EdgeSize - 2 * cellMargin, EdgeSize - 2 * cellMargin)));
                }
            }
        }


    }
}
