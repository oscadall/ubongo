﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using Ubongo.Model;

namespace Ubongo.View
{
    public class GameSpaceControl : Canvas
    {
        private bool initialBlockPlacement = true;
        private Point oldPoint;
        double origOffsetLeft;
        double origOffsetTop;
        private Size taskSize;

        private readonly TextBlock taskFinishedTextBlock = new TextBlock
        {
            Text = "Great!",
            Opacity = 0,
            FontFamily = new FontFamily("Persia BT"),
            FontSize = 120,
            Foreground = Brushes.White
        };

        public GameSpaceControl()
        {
            MaxWidth = 1200;
            MaxHeight = 900;
            SizeChanged += GameSpaceControl_SizeChanged;
            HorizontalAlignment = HorizontalAlignment.Stretch;
            VerticalAlignment = VerticalAlignment.Stretch;
        }

        public static readonly DependencyProperty GameSpaceProperty = DependencyProperty.Register(nameof(GameSpace),
            typeof(GameSpace),
            typeof(GameSpaceControl),
            new FrameworkPropertyMetadata(null, (s, a) =>
            {
                ((GameSpaceControl)s).OnGameSpaceChanged();
            }));

        public GameSpace GameSpace
        {
            get => (GameSpace)GetValue(GameSpaceProperty);
            set => SetValue(GameSpaceProperty, value);
        }

        public static readonly DependencyProperty EdgeSizeProperty = DependencyProperty.Register(nameof(EdgeSize),
            typeof(double),
            typeof(GameSpaceControl),
            new FrameworkPropertyMetadata(50.0,
                (s, a) =>
                {
                    ((GameSpaceControl)s).Redraw();
                }));

        public double EdgeSize
        {
            get => (double)GetValue(EdgeSizeProperty);
            set => SetValue(EdgeSizeProperty, value);
        }



        public static readonly DependencyProperty TableProperty = DependencyProperty.Register(nameof(Table),
            typeof(ImageSource),
            typeof(GameSpaceControl),
            new FrameworkPropertyMetadata(null,
                (s, a) =>
                {
                    ((GameSpaceControl)s).Redraw();
                }));

        public ImageSource Table
        {
            get => (ImageSource)GetValue(TableProperty);
            set => SetValue(TableProperty, value);
        }

        public static readonly DependencyProperty TaskFinishedCommandProperty = DependencyProperty.Register(nameof(TaskFinishedCommand),
            typeof(ICommand),
            typeof(GameSpaceControl),
            new FrameworkPropertyMetadata(null));

        public ICommand TaskFinishedCommand
        {
            get => (ICommand)GetValue(TaskFinishedCommandProperty);
            set => SetValue(TaskFinishedCommandProperty, value);
        }

        public static readonly DependencyProperty TaskFinishedCommandParameterProperty = DependencyProperty.Register(nameof(TaskFinishedCommandParameter),
            typeof(object),
            typeof(GameSpaceControl),
            new FrameworkPropertyMetadata(null));

        public object TaskFinishedCommandParameter
        {
            get => (object)GetValue(TaskFinishedCommandParameterProperty);
            set => SetValue(TaskFinishedCommandParameterProperty, value);
        }

        public TaskControl TaskControl { get; private set; }

        public List<BlockPolygon> BlockPolygons { get; } = new List<BlockPolygon>();

        public double TaskControlLeft
        {
            get
            {
                var r =  GetLeft(TaskControl);
                if (double.IsNaN(r))
                {
                    SetLeft(TaskControl, 0);
                    return 0;
                }
                return r;
            }
        }

        public double TaskControlTop
        {
            get
            {
                var r = GetTop(TaskControl);
                if (double.IsNaN(r))
                {
                    SetTop(TaskControl, 0);
                    return 0;
                }
                return  r;
            }
        }

        public void Init()
        {
            Children.Remove(taskFinishedTextBlock);
        }

        public void PlacementBlocks()
        {
            if(BlockPolygons.Count == 0)
                return;

            var horizontalSpace = 25;
            var verticalSpace = 25;

            if (BlockPolygons.Count < 5)
            {
                var width = BlockPolygons.Sum(p => p.Width + horizontalSpace) - horizontalSpace;
                var maxHeight = BlockPolygons.Max(p => p.Height);

                var lastX = (TaskControlLeft + (TaskControl.Width / 2)) - (width / 2);
                foreach (var polygon in BlockPolygons)
                {
                    var height = (TaskControlTop - verticalSpace - maxHeight) + ((maxHeight - polygon.Height) / 2);
                    SetTop(polygon, height);
                    SetLeft(polygon, lastX);
                    lastX += polygon.Width + horizontalSpace;
                }
            }
            else
            {
                int topCount = (BlockPolygons.Count % 2 == 0) ? BlockPolygons.Count / 2 : (BlockPolygons.Count + 1) / 2;

                double width = -horizontalSpace;
                double maxHeight = 0;
                for (int i = 0; i < topCount; i++)
                {
                    width += BlockPolygons[i].Width + horizontalSpace;
                    maxHeight = Math.Max(maxHeight, BlockPolygons[i].Height);
                }

                var lastX = (TaskControlLeft + (TaskControl.Width / 2)) - (width / 2);
                for (int i = 0; i < topCount; i++)
                {
                    var polygon = BlockPolygons[i];
                    var height = (TaskControlTop - verticalSpace - maxHeight) + ((maxHeight - polygon.Height) / 2);
                    SetTop(polygon, height);
                    SetLeft(polygon, lastX);
                    lastX += polygon.Width + horizontalSpace;
                }

                // bottom
                width = -horizontalSpace;
                maxHeight = 0;
                for (int i = topCount; i < BlockPolygons.Count; i++)
                {
                    width += BlockPolygons[i].Width + horizontalSpace;
                    maxHeight = Math.Max(maxHeight, BlockPolygons[i].Height);
                }

                lastX = (TaskControlLeft + (TaskControl.Width / 2)) - (width / 2);
                for (int i = topCount; i < BlockPolygons.Count; i++)
                {
                    var polygon = BlockPolygons[i];
                    var height = (TaskControlTop + verticalSpace + TaskControl.Height) + ((maxHeight - polygon.Height) / 2);
                    SetTop(polygon, height);
                    SetLeft(polygon, lastX);
                    lastX += polygon.Width + horizontalSpace;
                }
            }

        }

        public void OnGameSpaceChanged()
        {
            foreach (var polygon in BlockPolygons)
            {
                polygon.GotMouseCapture -= GameSpaceControl_GotMouseCapture;
                polygon.LostMouseCapture -= Polygon_LostMouseCapture;
            }
            Redraw();
        }

        public void Redraw()
        {
            if (GameSpace == null)
                return;

            Children.Clear();
            BlockPolygons.Clear();

            taskSize = new Size(EdgeSize * GameSpace.Width, EdgeSize * GameSpace.Height);
            TaskControl = new TaskControl(GameSpace, EdgeSize);
            Children.Add(TaskControl);

            Background = new ImageBrush { ImageSource = Table };

            foreach (var block in GameSpace.Blocks)
            {
                AddBlockPolygon(block);
            }

            initialBlockPlacement = true;
            Centering(ActualWidth, ActualHeight);
        }

        public void AddBlockPolygon(Block block)
        {
            var polygon = new BlockPolygon(block, EdgeSize);
            polygon.GotMouseCapture += GameSpaceControl_GotMouseCapture;
            polygon.LostMouseCapture += Polygon_LostMouseCapture;
            BlockPolygons.Add(polygon);
            Children.Add(polygon);
        }

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                var polygon = (BlockPolygon) sender;
                Point newPoint = e.GetPosition(this);
                SetLeft(polygon, origOffsetLeft + (newPoint.X - oldPoint.X));
                SetTop(polygon, origOffsetTop + (newPoint.Y - oldPoint.Y));
            }
        }

        protected void GameSpaceControl_GotMouseCapture(object sender, MouseEventArgs e)
        {
            var polygon = (BlockPolygon)sender;
            oldPoint = e.GetPosition(this);
            origOffsetLeft = GetLeft(polygon);
            origOffsetTop = GetTop(polygon);
            ((BlockPolygon)sender).MouseMove += OnMouseMove;
            SetAsMostUpperPolygon(polygon);
        }

        protected void Polygon_LostMouseCapture(object sender, MouseEventArgs e)
        {
            var blockPoygon = (BlockPolygon)sender;
            blockPoygon.MouseMove -= OnMouseMove;

            DropPolygon(blockPoygon);

            if(GameSpace.IsCorrectSolution())
                OnTaskFinished();
        }

        private void DropPolygon(BlockPolygon blockPolygon)
        {
            var posX = GetLeft(blockPolygon);
            var posY = GetTop(blockPolygon);
            var leftTopPos = GetLeftTopAfterRotation(blockPolygon);

            var centerX = posX + (blockPolygon.Width / 2);
            var centerY = posY + (blockPolygon.Height / 2);

            var transformPosition = blockPolygon.GetTransformedPosition(new Point(leftTopPos.X - centerX, leftTopPos.Y - centerY));

            var posTransX = transformPosition.X + centerX;
            var posTransY = transformPosition.Y + centerY;

            if (IsInGameSpace(posTransX, posTransY))
            {
                var x = (int)Math.Round((posTransX - TaskControlLeft) / EdgeSize);
                var y = (int)Math.Round((posTransY - TaskControlTop) / EdgeSize);

                var alignX = x * EdgeSize + TaskControlLeft;
                var alignY = y * EdgeSize + TaskControlTop;

                var diffX = posTransX - alignX;
                var diffY = posTransY - alignY;

                var targetX = posX - diffX;
                var targetY = posY - diffY;
                SetLeft(blockPolygon, targetX);
                SetTop(blockPolygon, targetY);
                blockPolygon.Block.SetPosition(x, y);
            }
            else
            {
                blockPolygon.Block.RemovePosition();
            }
        }

        private Point GetLeftTopAfterRotation(BlockPolygon blockPolygon)
        {
            switch (blockPolygon.Rotation)
            {
                case Rotation.Rotate0:
                    return new Point(GetLeft(blockPolygon), GetTop(blockPolygon));
                case Rotation.Rotate90:
                    return new Point(GetLeft(blockPolygon), GetTop(blockPolygon) + blockPolygon.Height);
                case Rotation.Rotate180:
                    return new Point(GetLeft(blockPolygon) + blockPolygon.Width, GetTop(blockPolygon) + blockPolygon.Height);
                case Rotation.Rotate270:
                    return new Point(GetLeft(blockPolygon) + blockPolygon.Width, GetTop(blockPolygon));
                default:
                    throw new ArgumentOutOfRangeException(nameof(blockPolygon.Rotation), blockPolygon.Rotation, null);
            }
        }

        private bool IsInGameSpace(double positionX, double positionY)
        {
            var left = TaskControlLeft - EdgeSize / 2;
            var top = TaskControlTop - EdgeSize / 2;
            var right = left + taskSize.Width + EdgeSize;
            var bottom = top + taskSize.Height + EdgeSize;
            return left <= positionX &&
                   top <= positionY &&
                   right >= positionX &&
                   bottom >= positionY;
        }


        private void GameSpaceControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Centering(e.NewSize.Width,e.NewSize.Height);
        }

        private void Centering(double width, double height)
        {
            if (TaskControl == null)
                return;

            var startLeft = TaskControlLeft;
            var startTop = TaskControlTop;
            SetTop(TaskControl, (height / 2) - (TaskControl.Height / 2));
            SetLeft(TaskControl, (width / 2) - (TaskControl.Width / 2));
            var diffLeft = GetLeft(TaskControl) - startLeft;
            var diffTop = GetTop(TaskControl) - startTop;

            if (initialBlockPlacement)
            {
                PlacementBlocks();
                initialBlockPlacement = false;
                return;
            }

            foreach (var polygon in BlockPolygons)
            {
                SetLeft(polygon, GetLeft(polygon) + diffLeft);
                SetTop(polygon, GetTop(polygon) + diffTop);
            }
        }

        private void AddFinishText()
        {
            Children.Add(taskFinishedTextBlock);
            taskFinishedTextBlock.Measure(new Size(ActualWidth, ActualHeight));
            var size = taskFinishedTextBlock.DesiredSize;
            SetLeft(taskFinishedTextBlock, (ActualWidth / 2) - (size.Width / 2));
            SetTop(taskFinishedTextBlock, (ActualHeight / 2) - (size.Height / 2));
        }

        private Task AnimateOpacityAsync(FrameworkElement element, double from, double to, int milliseconds)
        {
            var storyboard = new Storyboard();
            var animation = new DoubleAnimation
            {
                Duration = new Duration(TimeSpan.FromMilliseconds(milliseconds)),
                From = from,
                To = to,
            };
            Storyboard.SetTargetProperty(animation, new PropertyPath("Opacity"));
            storyboard.Children.Add(animation);
            storyboard.Begin(element);
            return Task.Delay(milliseconds);
        }

        private void AnimateOpacitySync(FrameworkElement element, double from, double to, int milliseconds)
        {
            var storyboard = new Storyboard();
            var animation = new DoubleAnimation
            {
                Duration = new Duration(TimeSpan.FromMilliseconds(milliseconds)),
                From = from,
                To = to,
            };
            Storyboard.SetTargetProperty(animation, new PropertyPath("Opacity"));
            storyboard.Children.Add(animation);
            storyboard.Begin(element);
        }

        private async void OnTaskFinished()
        {
            AnimateOpacitySync(TaskControl, 1, 0, 150);
            foreach (var polygon in BlockPolygons)
            {
                await AnimateOpacityAsync(polygon, 1, 0, 250);
            }

            AddFinishText();
            await AnimateOpacityAsync(taskFinishedTextBlock, 0, 1, 250);
            var _ = Task.Run(async () =>
            {
                await AnimateOpacityAsync(taskFinishedTextBlock, 1, 0, 300);
                Children.Remove(taskFinishedTextBlock);
            });

            GameSpaceReset();
            TaskFinishedCommand?.Execute(TaskFinishedCommandParameter);
        }

        private void GameSpaceReset()
        {
            foreach (var block in GameSpace.Blocks)
            {
                block.RemovePosition();
                block.RandomRotate();
            }
        }

        private void SetAsMostUpperPolygon(BlockPolygon polygon)
        {
            var zIndex = 1;
            BlockPolygons.Remove(polygon);
            BlockPolygons.Add(polygon);
            foreach (var blockPolygon in BlockPolygons)
            {
                SetZIndex(blockPolygon, zIndex++);
            }
        }
    }
}
