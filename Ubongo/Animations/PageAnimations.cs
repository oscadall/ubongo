﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace Ubongo.Animations
{
    public static class PageAnimations
    {
        public static async Task SlideFromRight(this Page page, int miliseconds)
        {
            var storyboard = new Storyboard();
            storyboard.AddSlideInFromRight(miliseconds, page.ActualWidth);
            storyboard.Begin(page);
            await Task.Delay(miliseconds);
        }

        public static async Task SlideFromLeft(this Page page, int miliseconds)
        {
            var storyboard = new Storyboard();
            storyboard.AddSlideInFromLeft(miliseconds, page.ActualWidth);
            storyboard.Begin(page);
            await Task.Delay(miliseconds);
        }

        public static async Task SlideFromUp(this Page page, int miliseconds)
        {
            var storyboard = new Storyboard();
            storyboard.AddSlideInFromUp(miliseconds, page.ActualWidth);
            storyboard.Begin(page);
            await Task.Delay(miliseconds);
        }

        public static async Task SlideFromDown(this Page page, int miliseconds)
        {
            var storyboard = new Storyboard();
            storyboard.AddSlideInFromRight(miliseconds, page.ActualWidth);
            storyboard.Begin(page);
            await Task.Delay(miliseconds);
        }

        public static async Task SlideToRight(this Page page, int miliseconds)
        {
            var storyboard = new Storyboard();
            storyboard.AddSlideOutToRight(miliseconds, page.ActualWidth);
            storyboard.Begin(page);
            await Task.Delay(miliseconds);
        }

        public static async Task SlideToLeft(this Page page, int miliseconds)
        {
            var storyboard = new Storyboard();
            storyboard.AddSlideOutToLeft(miliseconds, page.ActualWidth);
            storyboard.Begin(page);
            await Task.Delay(miliseconds);
        }

        public static async Task SlideToUp(this Page page, int miliseconds)
        {
            var storyboard = new Storyboard();
            storyboard.AddSlideOutToUp(miliseconds, page.ActualWidth);
            storyboard.Begin(page);
            await Task.Delay(miliseconds);
        }

        public static async Task SlideToDown(this Page page, int miliseconds)
        {
            var storyboard = new Storyboard();
            storyboard.AddSlideOutToRight(miliseconds, page.ActualWidth);
            storyboard.Begin(page);
            await Task.Delay(miliseconds);
        }
    }
}
