﻿using System;
using System.Windows;
using System.Windows.Media.Animation;

namespace Ubongo.Animations
{
    public static class StoryboardExtensions
    {
        private static void AddThicknessAnimationIn(this Storyboard storyboard, int miliseconds, double left, double top, double right, double bottom,
            double decelerationRation = 0.9f)
        {
            var animation = new ThicknessAnimation
            {
                Duration = new Duration(TimeSpan.FromMilliseconds(miliseconds)),
                To = new Thickness(0),
                From = new Thickness(left, top, right, bottom),
                DecelerationRatio = decelerationRation
            };

            Storyboard.SetTargetProperty(animation, new PropertyPath("Margin"));
            storyboard.Children.Add(animation);
        }

        private static void AddThicknessAnimationOut(this Storyboard storyboard, int miliseconds, double left, double top, double right, double bottom,
            double decelerationRation = 0.9f)
        {
            var animation = new ThicknessAnimation
            {
                Duration = new Duration(TimeSpan.FromMilliseconds(miliseconds)),
                From = new Thickness(0),
                To = new Thickness(left, top, right, bottom),
                DecelerationRatio = decelerationRation
            };

            Storyboard.SetTargetProperty(animation, new PropertyPath("Margin"));
            storyboard.Children.Add(animation);
        }

        public static void AddThicknessAnimation(this Storyboard storyboard, int miliseconds, Thickness from,
            Thickness to, double decelerationRation = 0.9f)
        {
            var animation = new ThicknessAnimation
            {
                Duration = new Duration(TimeSpan.FromMilliseconds(miliseconds)),
                From = from,
                To = to,
                DecelerationRatio = decelerationRation
            };

            Storyboard.SetTargetProperty(animation, new PropertyPath("Margin"));
            storyboard.Children.Add(animation);
        }

        public static void AddSlideInFromLeft(this Storyboard storyboard, int miliseconds, double offset,
            double decelerationRation = 0.9f)
        {
            AddThicknessAnimationIn(storyboard,miliseconds, -offset, 0, offset, 0, decelerationRation);
        }

        public static void AddSlideInFromRight(this Storyboard storyboard, int miliseconds, double offset,
            double decelerationRation = 0.9f)
        {
            AddThicknessAnimationIn(storyboard, miliseconds, offset, 0, -offset, 0, decelerationRation);
        }

        public static void AddSlideInFromUp(this Storyboard storyboard, int miliseconds, double offset,
            double decelerationRation = 0.9f)
        {
            AddThicknessAnimationIn(storyboard, miliseconds, 0, -offset, 0, offset, decelerationRation);
        }

        public static void AddSlideInFromDown(this Storyboard storyboard, int miliseconds, double offset,
            double decelerationRation = 0.9f)
        {
            AddThicknessAnimationIn(storyboard, miliseconds, 0, offset, 0, -offset, decelerationRation);
        }


        public static void AddSlideOutToLeft(this Storyboard storyboard, int miliseconds, double offset,
            double decelerationRation = 0.9f)
        {
            AddThicknessAnimationOut(storyboard, miliseconds, -offset, 0, offset, 0, decelerationRation);
        }

        public static void AddSlideOutToRight(this Storyboard storyboard, int miliseconds, double offset,
            double decelerationRation = 0.9f)
        {
            AddThicknessAnimationOut(storyboard, miliseconds, offset, 0, -offset, 0, decelerationRation);
        }

        public static void AddSlideOutToUp(this Storyboard storyboard, int miliseconds, double offset,
            double decelerationRation = 0.9f)
        {
            AddThicknessAnimationOut(storyboard, miliseconds, 0, -offset, 0, offset, decelerationRation);
        }

        public static void AddSlideOutToDown(this Storyboard storyboard, int miliseconds, double offset,
            double decelerationRation = 0.9f)
        {
            AddThicknessAnimationOut(storyboard, miliseconds, 0, offset, 0, -offset, decelerationRation);
        }



        public static void AddRotationAnimation(this Storyboard storyboard, int miliseconds, double from, double to, double decelerationRation = 0.9f)
        {
            var animation = new DoubleAnimation
            {
                Duration = new Duration(TimeSpan.FromMilliseconds(miliseconds)),
                From = from,
                To = to,
                DecelerationRatio = decelerationRation
            };

            Storyboard.SetTargetProperty(animation, new PropertyPath("RenderTransform.Children[0].Angle"));
            storyboard.Children.Add(animation);
        }

        public static void AddDoubleAnimation(this Storyboard storyboard, int miliseconds, double from, double to, PropertyPath propertyPath, double decelerationRation = 0.9f )
        {
            var animation = new DoubleAnimation
            {
                Duration = new Duration(TimeSpan.FromMilliseconds(miliseconds)),
                From = from,
                To = to,
                DecelerationRatio = decelerationRation
            };

            Storyboard.SetTargetProperty(animation, propertyPath);
            storyboard.Children.Add(animation);
        }


    }
}
