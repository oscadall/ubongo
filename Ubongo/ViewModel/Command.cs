﻿using System;
using System.Windows.Input;

namespace Ubongo.ViewModel
{
    public class Command<T> : ICommand
    {
        public Action<T> WhatToExecute { get; set; }
        private readonly Predicate<T> WhenToExecute;


        public Command(Action<T> What)
        {
            WhatToExecute = What;
        }

        public Command(Action<T> What, Predicate<T> When)
        {
            WhatToExecute = What;
            WhenToExecute = When;
        }

        public event EventHandler CanExecuteChanged;
        public void RaiseCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }

        public bool CanExecute(object parameter)
        {
            if (WhenToExecute == null)
                return true;

            return WhenToExecute((parameter == null) ? default(T) : (T)Convert.ChangeType(parameter, typeof(T)));
        }

        public void Execute(object parameter)
        {
            if (parameter == null)
            {
                WhatToExecute(default(T));
            }
            else if (parameter is T)
            {
                WhatToExecute((T)parameter);
            }
            else
            {
                WhatToExecute((T)Convert.ChangeType(parameter, typeof(T)));
            }
        }
    }

    public class Command : ICommand
    {

        public Action WhatToExecute { get; set; }
        private Func<bool> WhenToExecute;

        public event EventHandler CanExecuteChanged;
        public void RaiseCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }

        public Command(Action What)
        {
            WhatToExecute = What;
        }


        public Command(Action What, Func<bool> When)
        {
            WhatToExecute = What;
            WhenToExecute = When;
        }

        public bool CanExecute(object parameter)
        {
            return WhenToExecute != null ? WhenToExecute() : true;
        }

        public void Execute(object parameter)
        {
            WhatToExecute();
        }
    }
}
