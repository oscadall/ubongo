﻿using System;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Threading;

namespace Ubongo.ViewModel
{
    public abstract class PropertyChangedNotificator : INotifyPropertyChanged
    {
        private readonly ConcurrentDictionary<object, DispatcherOperation> dispatcherOperations = new ConcurrentDictionary<object, DispatcherOperation>();

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string name)
        {
            if (Thread.CurrentThread.GetApartmentState() == ApartmentState.STA)
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
            else if (Application.Current?.Dispatcher != null)
            {
                var action = new Action(() => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name)));
                if (!dispatcherOperations.ContainsKey(name))
                {
                    var act = Application.Current.Dispatcher.BeginInvoke(action);
                    dispatcherOperations.TryAdd(name, act);
                }
                else
                {
                    if (dispatcherOperations[name].Status == DispatcherOperationStatus.Pending)
                        dispatcherOperations[name].Abort();

                    dispatcherOperations[name] = Application.Current.Dispatcher.BeginInvoke(action);
                }
            }
        }
    }
}
