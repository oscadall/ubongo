﻿namespace Ubongo.Enums
{
    public enum GameCameraPositions
    {
        Login,
        MenuDifficulty,
        LevelsBeginner,
        LevelsMiddle,
        LevelsExpert,
        LevelsDevil,
        GameBeginner,
        GameMiddle,
        GameExpert,
        GameDevil,
    }

    
}
