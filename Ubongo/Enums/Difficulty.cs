﻿using System;

namespace Ubongo.Enums
{
    public enum Difficulty : int
    {
        Beginner = 0,
        Middle = 1,
        Expert = 2,
        Devil = 3,
    }

    public static class DifficultyExtensions
    {
        public static GameCameraPositions LevelsTileCameraPosition(this Difficulty difficulty)
        {
            switch (difficulty)
            {
                case Difficulty.Beginner:
                    return GameCameraPositions.LevelsBeginner;
                case Difficulty.Middle:
                    return GameCameraPositions.LevelsMiddle;
                case Difficulty.Expert:
                    return GameCameraPositions.LevelsExpert;
                case Difficulty.Devil:
                    return GameCameraPositions.LevelsDevil;
                default:
                    throw new ArgumentOutOfRangeException(nameof(difficulty), difficulty, null);
            }
        }

        public static GameCameraPositions GameTileCameraPosition(this Difficulty difficulty)
        {
            switch (difficulty)
            {
                case Difficulty.Beginner:
                    return GameCameraPositions.GameBeginner;
                case Difficulty.Middle:
                    return GameCameraPositions.GameMiddle;
                case Difficulty.Expert:        
                    return GameCameraPositions.GameExpert;
                case Difficulty.Devil:         
                    return GameCameraPositions.GameDevil;
                default:
                    throw new ArgumentOutOfRangeException(nameof(difficulty), difficulty, null);
            }
        }
    }
}
