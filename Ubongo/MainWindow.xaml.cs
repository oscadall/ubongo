﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Animation;
using Ubongo.Animations;
using Ubongo.Enums;
using Ubongo.View.World;

namespace Ubongo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private readonly Dictionary<GameCameraPositions, TilePosition> positions =
            new Dictionary<GameCameraPositions, TilePosition>
            {
                {GameCameraPositions.Login, new TilePosition(0, 1)},
                {GameCameraPositions.MenuDifficulty, new TilePosition(1, 1) },
                {GameCameraPositions.LevelsBeginner, new TilePosition(1,0)},
                {GameCameraPositions.GameBeginner, new TilePosition(0, 0)},
                {GameCameraPositions.LevelsMiddle, new TilePosition(1, 2)},
                {GameCameraPositions.GameMiddle, new TilePosition(0, 2)},
                {GameCameraPositions.LevelsExpert, new TilePosition(2, 1)},
                {GameCameraPositions.GameExpert, new TilePosition(2, 0)},
                {GameCameraPositions.LevelsDevil, new TilePosition(2, 2)},
                {GameCameraPositions.GameDevil, new TilePosition(2, 3)},
            };

        public MainWindow()
        {
            InitializeComponent();
            ViewModel.Init();
            SizeChanged += MainWindow_SizeChanged;
            ViewModel.PropertyChanged += ViewModel_PropertyChanged;
        }

        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if(e.PropertyName == nameof(MainWindowViewModel.ActualCameraPosition))
                AnimateMove(ViewModel.ActualCameraPosition);
        }

        private void MainWindow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            WorldControl.OnWindowSizeChanged(Width,Height);
            WorldControl.Margin = GetMarginForPosition(ViewModel.ActualCameraPosition);
        }

        public Thickness GetMarginForPosition(GameCameraPositions position)
        {
            var pos = positions[position];
            return  new Thickness
            {
                Left = ((WindowGrid.ActualWidth - WorldTile.TileWidth) / 2) - pos.LeftOffset,
                Top = ((WindowGrid.ActualHeight - WorldTile.TileHeight) / 2) - pos.TopOffset,
                Right = 0,
                Bottom = 0
            };
        }

        private async void AnimateMove(GameCameraPositions positions)
        {
            Storyboard storyboard = new Storyboard();
            storyboard.AddThicknessAnimation(2500, WorldControl.Margin, GetMarginForPosition(positions));
            storyboard.Begin(WorldControl);
            await Task.Delay(2500);
        }

    }

    public class TilePosition
    {
        public TilePosition(int row, int column)
        {
            Row = row;
            Column = column;
        }

        public int Row { get; }
        public int Column { get; }

        public int LeftOffset => WorldTile.TileWidth * Column;

        public int TopOffset => WorldTile.TileHeight * Row;
    }
}
