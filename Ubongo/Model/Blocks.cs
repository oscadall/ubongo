﻿using System;

namespace Ubongo.Model
{
    public class Blocks
    {
        public static Block A => new Block(nameof(A), new[,]
        {
            { true },
            { true }
        });

        public static Block B => new Block(nameof(B), new[,]
        {
            { true, false },
            { true, true }
        });

        public static Block C => new Block(nameof(C), new[,]
        {
            {true, true, false},
            {false, true, false},
            {false, true, true}
        });

        public static Block D => new Block(nameof(D), new[,]
        {
            {true, false},
            {true, true},
            {true, true}
        });

        public static Block E => new Block(nameof(E), new[,]
        {
            {true, false},
            {true, true},
            {true, false},
            {true, false}
        });

        public static Block F => new Block(nameof(F), new[,]
        {
            {true, false},
            {true, false},
            {true, false},
            {true, true}
        });


        public static Block G => new Block(nameof(G), new[,]
        {
            {true, false},
            {true, false},
            {true, true}
        });

        public static Block H => new Block(nameof(H), new[,]
        {
            {true},
            {true},
            {true},
            {true}
        });

        public static Block I => new Block(nameof(I), new[,]
        {
            {true, true},
            {true, true}
        });

        public static Block J => new Block(nameof(J), new[,]
        {
            {true, false},
            {true, true},
            {true, false}
        });

        public static Block K => new Block(nameof(K), new[,]
        {
            {true},
            {true},
            {true}
        });

        public static Block L => new Block(nameof(L), new[,]
        {
            {true , true, false},
            {false, true, true}
        });

        public static Block M => new Block(nameof(M), new[,]
        {
            { false, true , false },
            { true , true , true  },
            { false, true , false }
        });

        public static Block N => new Block(nameof(N), new[,]
        {
            { true , false, false },
            { true , true , true  },
            { false, true , false }
        });

        public static Block O => new Block(nameof(O), new[,]
        {
            { true , true , true  },
            { false, false, true  },
            { false, false, true  }
        });

        public static Block P => new Block(nameof(P), new[,]
        {
            { false, false, true  },
            { true , true , true  },
            { false, false, true  }
        });

        public static Block Q => new Block(nameof(Q), new[,]
        {
            { true , true , true  },
            { true , false, true  }
        });

        public static Block R => new Block(nameof(R), new[,]
        {
            { false, true , true  },
            { true , true , false },
            { true , false, false }
        });


        public static Block S => new Block(nameof(S), new[,]
        {
            { false, true },
            { true , true },
            { true , false},
            { true , false}
        });

        //public static Block T => new Block(new[,]
        //{

        //});
        public static Block NewBlock(string name)
        {
            switch (name.ToUpper())
            {
                case nameof(A): return A;
                case nameof(B): return B;
                case nameof(C): return C;
                case nameof(D): return D;
                case nameof(E): return E;
                case nameof(F): return F;
                case nameof(G): return G;
                case nameof(H): return H;
                case nameof(I): return I;
                case nameof(J): return J;
                case nameof(K): return K;
                case nameof(L): return L;
                case nameof(M): return M;
                case nameof(N): return N;
                case nameof(O): return O;
                case nameof(P): return P;
                case nameof(Q): return Q;
                case nameof(R): return R;
                case nameof(S): return S;
                default: throw new ArgumentOutOfRangeException();
            }
        }
    }
}
