﻿using System.Collections.Generic;

namespace Ubongo.Model
{
    public static class BaseTasks
    {
        private static GameSpace[] tasks = new GameSpace[]
        {
            // 1
            new GameSpace(new bool[,]
                {
                    {false, true, true, true},
                    {true, true, true, true},
                    {true, true, true, false},
                    {true, true, true, true},
                },
                new List<Block>
                {
                    Blocks.G,
                    Blocks.D,
                    Blocks.C,
                }),
            //new GameSpace(new bool[,]
            //    {
            //        { false, true , true  },	//   ████
            //        { true , true , true  },	// ██████
            //    },
            //    new List<Block>
            //    {
            //        Blocks.D,
            //    }),

            // 2
            new GameSpace(new bool[,]
                {
                    {true, false, false},
                    {true, true, true},
                    {true, true, true},
                    {true, true, true},
                    {false, true, false},
                    {false, true, false},
                },
                new List<Block>
                {
                    Blocks.B,
                    Blocks.C,
                    Blocks.G
                }),

            // 3
            new GameSpace(new bool[,]
                {
                    {true, true, true, true},
                    {true, true, true, true},
                    {true, true, true, true},
                    {true, false, false, false},
                },
                new List<Block>
                {
                    Blocks.L,
                    Blocks.F,
                    Blocks.G,
                }),

            // 4 
            new GameSpace(new bool[,]
                {
                    {true, true, false, false},
                    {true, true, true, false},
                    {true, true, true, true},
                    {true, true, true, true},
                    {false, false, false, true},
                },
                new List<Block>
                {
                    Blocks.C,
                    Blocks.G,
                    Blocks.D,
                }),

            // 5
            new GameSpace(new bool[,]
                {
                    {true, true, false, false},
                    {true, true, false, false},
                    {true, true, true, true},
                    {true, true, true, true},
                    {false, true, false, false},
                },
                new List<Block>
                {
                    Blocks.J,
                    Blocks.D,
                    Blocks.I
                }),


            // 6
            new GameSpace(new bool[,]
                {
                    {true, true, true, true},
                    {true, true, true, true},
                    {false, true, true, true},
                    {false, true, true, true},
                },
                new List<Block>
                {
                    Blocks.K,
                    Blocks.E,
                    Blocks.L,
                    Blocks.A
                }),

            // 7
            new GameSpace(new bool[,]
                {
                    {true, true, true, false, false},
                    {true, true, true, true, true},
                    {true, true, true, true, true},
                    {false, false, true, true, false},
                },
                new List<Block>
                {
                    Blocks.A,
                    Blocks.C,
                    Blocks.G,
                    Blocks.I,
                }),

            // 8
            new GameSpace(new bool[,]
                {
                    {true, true, false, false, false},
                    {true, true, true, true, true},
                    {true, true, true, true, true},
                    {true, true, true, true, true},
                },
                new List<Block>
                {
                    Blocks.G,
                    Blocks.B,
                    Blocks.C,
                    Blocks.D
                }),

            // 9
            new GameSpace(new bool[,]
                {
                    { false, true , true , true , true , false },
                    { false, true , true , true , true , false },
                    { true , true , true , true , true , false },
                    { false, false, false, true , true , true  },
                },
                new List<Block>
                {
                    Blocks.J,
                    Blocks.E,
                    Blocks.A,
                    Blocks.F
                }),


            // 10
            new GameSpace(new bool[,]
                {
                    { false, false, true , true , false },	//     ████  
                    { false, false, true , true , true  },	//     ██████
                    { false, false, true , true , true  },	//     ██████
                    { true , true , true , true , true  },	// ██████████
                    { false, true , true , true , true  },	//   ████████
                },
                new List<Block>
                {
                    Blocks.D,
                    Blocks.F,
                    Blocks.H,
                    Blocks.K,
                }),


            // 11
            new GameSpace(new bool[,]
                {
                    { false, false, true , false },	//     ██  
                    { false, false, true , true  },	//     ████
                    { true , true , true , true  },	// ████████
                    { true , true , true , true  },	// ████████
                    { false, true , true , true  },	//   ██████
                    { false, true , true , true  },	//   ██████
                },
                new List<Block>
                {
                    Blocks.F,
                    Blocks.I,
                    Blocks.J,
                    Blocks.L,
                }),

            
            // 12
            new GameSpace(new bool[,]
                {
                    { true , true , true , true  },	// ████████
                    { true , true , true , true  },	// ████████
                    { true , true , true , true  },	// ████████
                    { true , true , false, false },	// ████    
                    { true , false, false, false },	// ██      
                    { true , false, false, false },	// ██      
                },
                new List<Block>
                {
                    Blocks.A,
                    Blocks.D,
                    Blocks.E,
                    Blocks.H,
                }),


            // 13
            new GameSpace(new bool[,]
                {
                    { false, false, false, false, true  },	//         ██
                    { true , true , true , true , true  },	// ██████████
                    { false, true , true , true , true  },	//   ████████
                    { false, true , true , true , true  },	//   ████████
                    { false, false, false, true , true  },	//       ████
                    { false, false, false, true , true  },	//       ████
                },
                new List<Block>
                {
                    Blocks.E,
                    Blocks.F,
                    Blocks.I,
                    Blocks.L,
                }),


            // 14
            new GameSpace(new bool[,]
                {
                    { false, false, true , false },	//     ██  
                    { false, true , true , true  },	//   ██████
                    { false, true , true , true  },	//   ██████
                    { true , true , true , true  },	// ████████
                    { true , true , true , true  },	// ████████
                    { true , true , false, false },	// ████    
                },
                new List<Block>
                {
                    Blocks.E,
                    Blocks.G,
                    Blocks.I,
                    Blocks.L,
                }),


            // 15
            new GameSpace(new bool[,]
                {
                    { false, false, true , false, false },	//     ██    
                    { true , true , true , true , true  },	// ██████████
                    { true , true , true , true , true  },	// ██████████
                    { true , true , true , true , false },	// ████████  
                },
                new List<Block>
                {
                    Blocks.B,
                    Blocks.C,
                    Blocks.J,
                    Blocks.K,
                }),


            // 16
            new GameSpace(new bool[,]
                {
                    { false, true , false, false },	//   ██    
                    { true , true , true , true  },	// ████████
                    { true , true , true , true  },	// ████████
                    { true , true , true , true  },	// ████████
                    { false, false, true , true  },	//     ████
                    { false, false, true , false },	//     ██  
                },
                new List<Block>
                {
                    Blocks.F,
                    Blocks.G,
                    Blocks.H,
                    Blocks.K,
                }),


            // 17
            new GameSpace(new bool[,]
                {
                    { false, false, true , true , true  },	//     ██████
                    { true , true , true , true , false },	// ████████  
                    { true , true , true , true , true  },	// ██████████
                    { true , true , true , true , false },	// ████████  
                    { false, false, false, true , false },	//       ██  
                },
                new List<Block>
                {
                    Blocks.A,
                    Blocks.C,
                    Blocks.E,
                    Blocks.F,
                }),


            // 18
            new GameSpace(new bool[,]
                {
                    { false, false, true , true , true  },	//     ██████
                    { false, false, true , true , true  },	//     ██████
                    { false, false, true , true , true  },	//     ██████
                    { false, false, true , true , true  },	//     ██████
                    { true , true , true , true , true  },	// ██████████
                },
                new List<Block>
                {
                    Blocks.C,
                    Blocks.D,
                    Blocks.K,
                    Blocks.L,
                }),


            // 19
            new GameSpace(new bool[,]
                {
                    { true , false, false, true , true , true  },	// ██    ██████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , false },	// ██████████  
                    { false, false, false, true , true , false },	//       ████  
                },
                new List<Block>
                {
                    Blocks.C,
                    Blocks.D,
                    Blocks.K,
                    Blocks.L,
                }),


            // 20
            new GameSpace(new bool[,]
                {
                    { true , true , true , true  },	// ████████
                    { true , true , true , true  },	// ████████
                    { true , true , true , true  },	// ████████
                    { true , true , true , false },	// ██████  
                },
                new List<Block>
                {
                    Blocks.B,
                    Blocks.E,
                    Blocks.G,
                    Blocks.K,
                }),


            // 21
            new GameSpace(new bool[,]
                {
                    { false, false, true , true , false },	//     ████  
                    { false, true , true , true , false },	//   ██████  
                    { true , true , true , true , false },	// ████████  
                    { true , true , true , true , false },	// ████████  
                    { false, false, true , true , true  },	//     ██████
                },
                new List<Block>
                {
                    Blocks.A,
                    Blocks.C,
                    Blocks.E,
                    Blocks.L,
                }),


            // 22
            new GameSpace(new bool[,]
                {
                    { false, true , true , true , false },	//   ██████  
                    { true , true , true , true , false },	// ████████  
                    { true , true , true , true , true  },	// ██████████
                    { true , true , true , true , true  },	// ██████████
                },
                new List<Block>
                {
                    Blocks.D,
                    Blocks.E,
                    Blocks.I,
                    Blocks.K,
                }),


            // 23
            new GameSpace(new bool[,]
                {
                    { true , false, false, false },	// ██      
                    { true , true , true , true  },	// ████████
                    { true , true , true , false },	// ██████  
                    { true , true , true , true  },	// ████████
                    { false, true , true , true  },	//   ██████
                    { false, true , true , true  },	//   ██████
                },
                new List<Block>
                {
                    Blocks.D,
                    Blocks.E,
                    Blocks.G,
                    Blocks.H,
                }),


            // 24
            new GameSpace(new bool[,]
                {
                    { true , true , true , true , true  },	// ██████████
                    { true , true , true , true , false },	// ████████  
                    { true , true , true , true , false },	// ████████  
                    { true , true , true , true , false },	// ████████  
                    { false, true , false, false, false },	//   ██      
                },
                new List<Block>
                {
                    Blocks.C,
                    Blocks.E,
                    Blocks.G,
                    Blocks.I,
                }),


            // 25
            new GameSpace(new bool[,]
                {
                    { false, false, true , false, false, false },	//     ██      
                    { false, true , true , true , true , false },	//   ████████  
                    { true , true , true , true , true , false },	// ██████████  
                    { true , true , true , true , true , false },	// ██████████  
                    { true , true , true , true , true , true  },	// ████████████
                    { false, true , true , true , false, false },	//   ██████    
                },
                new List<Block>
                {
                    Blocks.L,
                    Blocks.M,
                    Blocks.N,
                    Blocks.P,
                    Blocks.Q,
                }),

            // 26
            new GameSpace(new bool[,]
                {
                    { false, false, true , false, false, false },	//     ██      
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { false, false, false, true , true , true  },	//       ██████
                },
                new List<Block>
                {
                    Blocks.M,
                    Blocks.C,
                    Blocks.J,
                    Blocks.G,
                    Blocks.J,
                }),

            // 27
            new GameSpace(new bool[,]
                {
                    { false, true , true , true , true  },	//   ████████
                    { true , true , true , true , true  },	// ██████████
                    { true , true , true , true , true  },	// ██████████
                    { true , true , true , true , true  },	// ██████████
                },
                new List<Block>
                {
                    Blocks.A,
                    Blocks.G,
                    Blocks.F,
                    Blocks.D,
                    Blocks.B,
                }),

            // 28
            new GameSpace(new bool[,]
                {
                    { false, false, false, true , false },	//       ██  
                    { true , true , true , true , true  },	// ██████████
                    { true , true , true , true , true  },	// ██████████
                    { true , true , true , true , true  },	// ██████████
                    { true , true , true , true , true  },	// ██████████
                },
                new List<Block>
                {
                    Blocks.P,
                    Blocks.L,
                    Blocks.I,
                    Blocks.H,
                    Blocks.J,
                }),

            // 29
            new GameSpace(new bool[,]
                {
                    { false, true , true , false, false },	//   ████    
                    { true , true , true , true , true  },	// ██████████
                    { true , true , true , true , true  },	// ██████████
                    { true , true , true , true , true  },	// ██████████
                    { true , true , true , true , false },	// ████████  
                },
                new List<Block>
                {
                    Blocks.R,
                    Blocks.R,
                    Blocks.R,
                    Blocks.B,
                    Blocks.B,
                }),

            // 30
            new GameSpace(new bool[,]
                {
                    { false, true , true , true , true  },	//   ████████
                    { false, true , true , true , true  },	//   ████████
                    { false, true , true , true , true  },	//   ████████
                    { true , true , true , true , true  },	// ██████████
                    { true , true , true , false, false },	// ██████    
                },
                new List<Block>
                {
                    Blocks.I,
                    Blocks.E,
                    Blocks.G,
                    Blocks.D,
                    Blocks.A,
                }),

            // 31
            new GameSpace(new bool[,]
                {
                    { false, false, true , false, false },	//     ██    
                    { true , true , true , true , false },	// ████████  
                    { true , true , true , true , false },	// ████████  
                    { true , true , true , true , true  },	// ██████████
                    { true , true , true , true , true  },	// ██████████
                    { false, false, true , true , false },	//     ████  
                },
                new List<Block>
                {
                    Blocks.C,
                    Blocks.G,
                    Blocks.G,
                    Blocks.R,
                    Blocks.B,
                }),

            // 32
            new GameSpace(new bool[,]
                {
                    { false, false, true , true , true , false },	//     ██████  
                    { false, true , true , true , true , true  },	//   ██████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                },
                new List<Block>
                {
                    Blocks.L,
                    Blocks.H,
                    Blocks.J,
                    Blocks.R,
                    Blocks.B,
                }),

            // 33
            new GameSpace(new bool[,]
                {
                    { true , true , false, false, false },	// ████      
                    { true , true , true , true , false },	// ████████  
                    { true , true , true , true , true  },	// ██████████
                    { true , true , true , true , true  },	// ██████████
                    { true , true , true , true , false },	// ████████  
                },
                new List<Block>
                {
                    Blocks.G,
                    Blocks.G,
                    Blocks.G,
                    Blocks.G,
                    Blocks.G,
                }),

            // 34
            new GameSpace(new bool[,]
                {
                    { false, true , true , true , true , false, false },	//   ████████    
                    { false, true , true , true , true , false, false },	//   ████████    
                    { false, true , true , true , true , false, false },	//   ████████    
                    { true , true , true , true , true , true , false },	// ████████████  
                    { false, false, true , true , true , true , true  },	//     ██████████
                },
                new List<Block>
                {
                    Blocks.D,
                    Blocks.F,
                    Blocks.G,
                    Blocks.C,
                    Blocks.J,
                }),

            // 35
            new GameSpace(new bool[,]
                {
                    { false, true , true , true , false, false },	//   ██████    
                    { false, true , true , true , true , false },	//   ████████  
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                },
                new List<Block>
                {
                    Blocks.N,
                    Blocks.Q,
                    Blocks.J,
                    Blocks.B,
                    Blocks.A,
                }),

            // 36
            new GameSpace(new bool[,]
                {
                    { false, false, true , true , false, false },	//     ████    
                    { false, true , true , true , true , false },	//   ████████  
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { false, true , true , true , false, true  },	//   ██████  ██
                },
                new List<Block>
                {
                    Blocks.L,
                    Blocks.C,
                    Blocks.C,
                    Blocks.L,
                    Blocks.L,
                }),

            // 37
            new GameSpace(new bool[,]
                {
                    { true , true , true , true , false, false },	// ████████    
                    { true , true , true , true , false, false },	// ████████    
                    { false, true , true , true , false, false },	//   ██████    
                    { false, true , true , true , true , false },	//   ████████  
                    { false, true , false, true , true , true  },	//   ██  ██████
                },
                new List<Block>
                {
                    Blocks.H,
                    Blocks.M,
                    Blocks.L,
                    Blocks.A,
                    Blocks.G,
                }),

            // 38
            new GameSpace(new bool[,]
                {
                    { true , true , true , true , true  },	// ██████████
                    { true , true , true , true , true  },	// ██████████
                    { true , true , true , true , true  },	// ██████████
                    { true , true , true , true , true  },	// ██████████
                    { true , true , true , true , true  },	// ██████████
                },
                new List<Block>
                {
                    Blocks.D,
                    Blocks.C,
                    Blocks.Q,
                    Blocks.Q,
                    Blocks.D,
                }),

            // 39
            new GameSpace(new bool[,]
                {
                    { false, false, false, true , false, false },	//       ██    
                    { false, true , true , true , true , true  },	//   ██████████
                    { false, true , true , true , true , true  },	//   ██████████
                    { true , true , true , true , true , true  },	// ████████████
                    { false, true , true , true , true , true  },	//   ██████████
                },
                new List<Block>
                {
                    Blocks.E,
                    Blocks.F,
                    Blocks.L,
                    Blocks.N,
                    Blocks.K,
                }),

            // 40
            new GameSpace(new bool[,]
                {
                    { false, true , true , true , true , true  },	//   ██████████
                    { true , true , true , true , true , false },	// ██████████  
                    { true , true , true , true , true , false },	// ██████████  
                    { true , true , true , true , true , false },	// ██████████  
                },
                new List<Block>
                {
                    Blocks.F,
                    Blocks.G,
                    Blocks.B,
                    Blocks.B,
                    Blocks.F,
                }),

            // 41
            new GameSpace(new bool[,]
                {
                    { false, true , false, false, false, false },	//   ██        
                    { true , true , true , true , true , false },	// ██████████  
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , false },	// ██████████  
                },
                new List<Block>
                {
                    Blocks.J,
                    Blocks.D,
                    Blocks.M,
                    Blocks.L,
                    Blocks.D,
                }),

            // 42
            new GameSpace(new bool[,]
                {
                    { false, true , false, true , true , false },	//   ██  ████  
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , false },	// ██████████  
                    { true , false, true , true , true , false },	// ██  ██████  
                    { false, false, true , true , true , false },	//     ██████  
                },
                new List<Block>
                {
                    Blocks.A,
                    Blocks.C,
                    Blocks.D,
                    Blocks.E,
                    Blocks.L,
                }),

            // 43
            new GameSpace(new bool[,]
                {
                    { true , true , true , true , true , true  },	// ████████████
                    { false, true , true , true , true , true  },	//   ██████████
                    { true , true , true , true , true , false },	// ██████████  
                    { true , true , true , true , true , false },	// ██████████  
                },
                new List<Block>
                {
                    Blocks.I,
                    Blocks.D,
                    Blocks.J,
                    Blocks.G,
                    Blocks.G,
                }),

            // 44
            new GameSpace(new bool[,]
                {
                    { false, false, true , true , true  },	//     ██████
                    { true , true , true , true , true  },	// ██████████
                    { true , true , false, true , true  },	// ████  ████
                    { true , true , true , true , true  },	// ██████████
                    { false, true , true , true , false },	//   ██████  
                },
                new List<Block>
                {
                    Blocks.O,
                    Blocks.R,
                    Blocks.A,
                    Blocks.G,
                    Blocks.G,
                }),

            // 45
            new GameSpace(new bool[,]
                {
                    { true , true , true , false, false, false },	// ██████      
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , false, true  },	// ████████  ██
                },
                new List<Block>
                {
                    Blocks.L,
                    Blocks.N,
                    Blocks.I,
                    Blocks.B,
                    Blocks.J,
                }),

            // 46
            new GameSpace(new bool[,]
                {
                    { false, false, true , true , true , true , true  },	//     ██████████
                    { false, true , true , true , true , true , true  },	//   ████████████
                    { false, true , true , true , true , true , true  },	//   ████████████
                    { true , true , true , true , true , true , false },	// ████████████  
                },
                new List<Block>
                {
                    Blocks.E,
                    Blocks.D,
                    Blocks.G,
                    Blocks.E,
                    Blocks.L,
                }),

            // 47
            new GameSpace(new bool[,]
                {
                    { false, false, true , true , true , false },	//     ██████  
                    { true , true , true , false, true , false },	// ██████  ██  
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { false, true , true , true , false, true  },	//   ██████  ██
                },
                new List<Block>
                {
                    Blocks.C,
                    Blocks.C,
                    Blocks.B,
                    Blocks.C,
                    Blocks.C,
                }),

            // 48
            new GameSpace(new bool[,]
                {
                    { true , true , true , true , true , false },	// ██████████  
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , false },	// ██████████  
                    { true , true , true , true , true , false },	// ██████████  
                    { false, true , true , true , false, false },	//   ██████    
                },
                new List<Block>
                {
                    Blocks.N,
                    Blocks.N,
                    Blocks.N,
                    Blocks.N,
                    Blocks.I,
                }),

            // 49
            new GameSpace(new bool[,]
                {
                    { true , true , true , true , true , true  },	// ████████████
                    { false, true , true , true , true , false },	//   ████████  
                    { true , true , true , true , true , false },	// ██████████  
                    { true , true , true , true , true , true  },	// ████████████
                    { false, false, false, true , true , true  },	//       ██████
                },
                new List<Block>
                {
                    Blocks.J,
                    Blocks.D,
                    Blocks.D,
                    Blocks.D,
                    Blocks.D,
                }),
            
           

        };

        public static GameSpace GetTask(int index)
        {
            return tasks[index];
        }

        //private static GameSpace CreateGameSpace(bool[][,] definitions)
        //{
        //    var blocks = new List<Block>();
        //    var gameSpace = new GameSpace(definitions[0], blocks);

        //    for (int i = 1; i < definitions.Length; i++)
        //    {
        //        blocks.Add(new Block(definitions[i]));
        //    }

        //    return gameSpace;
        //}
    }
}
