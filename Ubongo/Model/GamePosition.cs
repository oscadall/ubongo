﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Ubongo.Enums;
using Ubongo.ViewModel;

namespace Ubongo.Model
{
    public class GamePosition : PropertyChangedNotificator
    {
        // for Bson
        public GamePosition() { }

        public GamePosition(string playerName)
        {
            ActualOpenedLevels = new bool[4][];
            ActualOpenedLevels[(int)Difficulty.Beginner] = new bool[OpenedNewLevels];
            ActualOpenedLevels[(int)Difficulty.Middle] = new bool[OpenedNewLevels];
            ActualOpenedLevels[(int)Difficulty.Expert] = new bool[OpenedNewLevels];
            ActualOpenedLevels[(int)Difficulty.Devil] = new bool[6];
            Player = playerName;
            Position = new []{ 1, 1, 1, 1};
        }

        public const int OpenedNewLevels = 6;

        public string Player { get; set; }

        public int[] Position { get; set; }

        public bool[][] ActualOpenedLevels { get; set; }

#if !DEBUG
        public bool IsMiddleOpen => IsLevelOpen(49, Difficulty.Beginner);
        public bool IsExpertOpen => IsLevelOpen(49, Difficulty.Middle);
        public bool IsDevilOpen => IsLevelOpen(49, Difficulty.Expert);
#else
        public bool IsMiddleOpen => true;
        public bool IsExpertOpen => true;
        public bool IsDevilOpen => true;
#endif

        public void SetAsSolved(int level, Difficulty difficulty)
        {
            var diff = (int)difficulty;
            var position = Position[diff];
            if (level < position)
                return;

            if(level > position + OpenedNewLevels)
                throw new ArgumentOutOfRangeException(nameof(level));


            // position 31, level 33
            ActualOpenedLevels[diff][level - position] = true;
            OnPropertyChanged(nameof(ActualOpenedLevels));

            if (ActualOpenedLevels[diff].All(l => l))
            {
                Position[diff] += OpenedNewLevels;
                ActualOpenedLevels[diff] = new bool[OpenedNewLevels];
                OnPropertyChanged(nameof(ActualOpenedLevels));
                if (Position[diff] > 48)
                {
                    OnPropertyChanged(nameof(IsMiddleOpen));
                    OnPropertyChanged(nameof(IsExpertOpen));
                    OnPropertyChanged(nameof(IsDevilOpen));
                }
            }
        }

        public bool IsLevelOpen(int level, Difficulty difficulty)
        {
            return level <= Position[(int)difficulty] + OpenedNewLevels - 1;
        }

        public bool IsLevelSolved(int level, Difficulty difficulty)
        {
            var diff = (int)difficulty;
            return level < Position[diff] || (level < Position[diff] + OpenedNewLevels && ActualOpenedLevels[diff][level - Position[diff]]);
        }

        public void Save(string path)
        {
            File.WriteAllBytes(path + "/" + Player.GetHashCode(), ToBytes());
        }

        public void DeleteFrom(string path)
        {
            File.Delete(path + "/" + Player.GetHashCode());
        }

        public byte[] ToBytes()
        {
            string json = JsonConvert.SerializeObject(this, Formatting.Indented);
            return Encoding.ASCII.GetBytes(json);
        }

    }


    public static class GamePositonExtension
    {
        public static GamePosition ConvertToGamePosition(this byte[] data)
        {
            return JsonConvert.DeserializeObject<GamePosition>(Encoding.ASCII.GetString(data));
        }
    }
}
