﻿using System.Collections.Generic;

namespace Ubongo.Model
{
    public class ExpertTasks
    {
        private static GameSpace[] tasks = new GameSpace[]
        {
            // 1
            new GameSpace(new[,]
                {
                    { false, true , true , true , false, false },	//   ██████    
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , false, true , true  },	// ██████  ████
                },
                new List<Block>
                {
                    Blocks.C,
                    Blocks.E,
                    Blocks.N,
                    Blocks.D,
                    Blocks.B,
                    Blocks.N,
                    Blocks.L,
                }),

            // 2
            new GameSpace(new[,]
                {
                    { false, true , true , true , false, false, false },	//   ██████      
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                },
                new List<Block>
                {
                    Blocks.O,
                    Blocks.O,
                    Blocks.Q,
                    Blocks.J,
                    Blocks.F,
                    Blocks.K,
                    Blocks.L,
                }),

            // 3
            new GameSpace(new[,]
                {
                    { false, false, false, false, false, false, true , false },	//             ██  
                    { false, false, true , true , true , true , true , false },	//     ██████████  
                    { true , true , true , true , true , true , true , true  },	// ████████████████
                    { true , true , true , true , true , true , true , true  },	// ████████████████
                    { true , true , true , true , true , true , true , true  },	// ████████████████
                },
                new List<Block>
                {
                    Blocks.M,
                    Blocks.K,
                    Blocks.G,
                    Blocks.G,
                    Blocks.E,
                    Blocks.G,
                    Blocks.D,
                }),

            // 4
            new GameSpace(new[,]
                {
                    { false, true , true , true , true , true , false },	//   ██████████  
                    { true , true , true , true , true , true , false },	// ████████████  
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { false, true , true , true , true , true , true  },	//   ████████████
                    { false, true , true , true , true , true , true  },	//   ████████████
                },
                new List<Block>
                {
                    Blocks.L,
                    Blocks.O,
                    Blocks.O,
                    Blocks.B,
                    Blocks.M,
                    Blocks.R,
                    Blocks.K,
                }),

            // 5
            new GameSpace(new[,]
                {
                    { false, false, true , true , true , false, false },	//     ██████    
                    { false, false, true , true , true , false, false },	//     ██████    
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { false, false, true , true , true , false, false },	//     ██████    
                },
                new List<Block>
                {
                    Blocks.C,
                    Blocks.A,
                    Blocks.Q,
                    Blocks.Q,
                    Blocks.M,
                    Blocks.D,
                    Blocks.K,
                }),

            // 6
            new GameSpace(new[,]
                {
                    { false, false, false, false, false, true , false, false },	//           ██    
                    { false, false, true , true , true , true , true , false },	//     ██████████  
                    { true , true , true , true , true , true , true , true  },	// ████████████████
                    { true , true , true , true , true , true , true , false },	// ██████████████  
                    { true , true , true , true , true , true , true , false },	// ██████████████  
                    { true , true , true , true , true , true , false, false },	// ████████████    
                },
                new List<Block>
                {
                    Blocks.R,
                    Blocks.R,
                    Blocks.P,
                    Blocks.O,
                    Blocks.L,
                    Blocks.M,
                    Blocks.N,
                }),

            // 7
            new GameSpace(new[,]
                {
                    { false, true , true , true , true , true , false },	//   ██████████  
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , false, true , true , true , true , true  },	// ██  ██████████
                },
                new List<Block>
                {
                    Blocks.Q,
                    Blocks.N,
                    Blocks.J,
                    Blocks.R,
                    Blocks.F,
                    Blocks.R,
                    Blocks.K,
                }),

            // 8
            new GameSpace(new[,]
                {
                    { false, false, true , true , true , true , false },	//     ████████  
                    { false, true , true , true , true , true , true  },	//   ████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                },
                new List<Block>
                {
                    Blocks.F,
                    Blocks.G,
                    Blocks.J,
                    Blocks.R,
                    Blocks.N,
                    Blocks.D,
                    Blocks.B,
                }),

            // 9
            new GameSpace(new[,]
                {
                    { false, true , true , true , true , false },	//   ████████  
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { false, true , true , true , true , false },	//   ████████  
                    { false, false, true , true , false, false },	//     ████    
                },
                new List<Block>
                {
                    Blocks.M,
                    Blocks.L,
                    Blocks.M,
                    Blocks.Q,
                    Blocks.K,
                    Blocks.G,
                    Blocks.A,
                }),

            // 10
            new GameSpace(new[,]
                {
                    { false, true , true , false, true , true , false },	//   ████  ████  
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { false, true , true , true , true , true , false },	//   ██████████  
                    { false, false, true , true , true , false, false },	//     ██████    
                    { false, false, false, true , false, false, false },	//       ██      
                },
                new List<Block>
                {
                    Blocks.R,
                    Blocks.D,
                    Blocks.G,
                    Blocks.E,
                    Blocks.R,
                    Blocks.M,
                    Blocks.D,
                }),

            // 11
            new GameSpace(new[,]
                {
                    { false, false, true , true , false, false },	//     ████    
                    { false, true , true , true , true , false },	//   ████████  
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                },
                new List<Block>
                {
                    Blocks.L,
                    Blocks.K,
                    Blocks.G,
                    Blocks.F,
                    Blocks.I,
                    Blocks.E,
                    Blocks.D,
                }),

            // 12
            new GameSpace(new[,]
                {
                    { false, false, true , true , false, false, false },	//     ████      
                    { true , true , true , true , true , true , false },	// ████████████  
                    { true , true , true , false, true , true , false },	// ██████  ████  
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , false },	// ████████████  
                    { false, true , true , true , true , true , false },	//   ██████████  
                },
                new List<Block>
                {
                    Blocks.S,
                    Blocks.M,
                    Blocks.J,
                    Blocks.L,
                    Blocks.D,
                    Blocks.C,
                    Blocks.B,
                }),

            // 13
            new GameSpace(new[,]
                {
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { false, true , true , true , true , true  },	//   ██████████
                    { false, true , true , true , true , true  },	//   ██████████
                    { false, false, false, false, true , false },	//         ██  
                },
                new List<Block>
                {
                    Blocks.S,
                    Blocks.L,
                    Blocks.C,
                    Blocks.E,
                    Blocks.I,
                    Blocks.G,
                    Blocks.A,
                }),

            // 14
            new GameSpace(new[,]
                {
                    { false, true , true , true , false, false, false },	//   ██████      
                    { true , true , true , true , true , true , false },	// ████████████  
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , false },	// ████████████  
                    { false, true , true , true , true , true , true  },	//   ████████████
                },
                new List<Block>
                {
                    Blocks.K,
                    Blocks.M,
                    Blocks.C,
                    Blocks.A,
                    Blocks.L,
                    Blocks.G,
                    Blocks.Q,
                }),

            // 15
            new GameSpace(new[,]
                {
                    { false, false, false, false, true , true , false },	//         ████  
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                },
                new List<Block>
                {
                    Blocks.H,
                    Blocks.O,
                    Blocks.F,
                    Blocks.D,
                    Blocks.A,
                    Blocks.P,
                    Blocks.I,
                }),

            // 16
            new GameSpace(new[,]
                {
                    { false, false, true , true , true , true  },	//     ████████
                    { false, true , true , true , true , true  },	//   ██████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                },
                new List<Block>
                {
                    Blocks.S,
                    Blocks.C,
                    Blocks.B,
                    Blocks.B,
                    Blocks.J,
                    Blocks.K,
                    Blocks.H,
                }),

            // 17
            new GameSpace(new[,]
                {
                    { false, false, false, true , true , false, false },	//       ████    
                    { false, true , true , true , true , true , true  },	//   ████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , false },	// ████████████  
                    { false, false, true , true , false, false, false },	//     ████      
                },
                new List<Block>
                {
                    Blocks.D,
                    Blocks.I,
                    Blocks.M,
                    Blocks.B,
                    Blocks.N,
                    Blocks.G,
                    Blocks.L,
                }),

            // 18
            new GameSpace(new[,]
                {
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , false, true  },	// ████████  ██
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { false, true , true , true , true , true  },	//   ██████████
                },
                new List<Block>
                {
                    Blocks.J,
                    Blocks.B,
                    Blocks.Q,
                    Blocks.D,
                    Blocks.O,
                    Blocks.B,
                    Blocks.K,
                }),

            // 19
            new GameSpace(new[,]
                {
                    { false, false, true , true , true , true , true  },	//     ██████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , false, true , true , true  },	// ██████  ██████
                    { false, true , true , true , true , true , true  },	//   ████████████
                    { false, true , true , true , true , true , true  },	//   ████████████
                },
                new List<Block>
                {
                    Blocks.G,
                    Blocks.E,
                    Blocks.D,
                    Blocks.J,
                    Blocks.H,
                    Blocks.K,
                    Blocks.S,
                }),

            // 20
            new GameSpace(new[,]
                {
                    { true , true , true , true , true , true , false },	// ████████████  
                    { true , true , true , true , true , true , false },	// ████████████  
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { false, true , true , true , true , true , false },	//   ██████████  
                    { false, true , true , true , true , true , false },	//   ██████████  
                },
                new List<Block>
                {
                    Blocks.I,
                    Blocks.E,
                    Blocks.L,
                    Blocks.F,
                    Blocks.H,
                    Blocks.J,
                    Blocks.B,
                }),

            // 21
            new GameSpace(new[,]
                {
                    { false, true , true , true , true , true , true  },	//   ████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , false },	// ████████████  
                    { true , true , true , true , true , true , false },	// ████████████  
                    { true , true , true , true , true , true , true  },	// ██████████████
                },
                new List<Block>
                {
                    Blocks.S,
                    Blocks.S,
                    Blocks.G,
                    Blocks.J,
                    Blocks.D,
                    Blocks.P,
                    Blocks.L,
                }),

            // 22
            new GameSpace(new[,]
                {
                    { false, true , true , true , true , true , true  },	//   ████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , false },	// ████████████  
                },
                new List<Block>
                {
                    Blocks.N,
                    Blocks.R,
                    Blocks.I,
                    Blocks.F,
                    Blocks.C,
                    Blocks.Q,
                    Blocks.L,
                }),

            // 23
            new GameSpace(new[,]
                {
                    { false, false, false, true , true , false, false, false },	//       ████      
                    { false, false, true , true , true , true , true , false },	//     ██████████  
                    { false, true , true , true , true , true , true , false },	//   ████████████  
                    { true , true , true , true , true , true , true , true  },	// ████████████████
                    { true , true , true , true , true , true , false, false },	// ████████████    
                },
                new List<Block>
                {
                    Blocks.A,
                    Blocks.K,
                    Blocks.H,
                    Blocks.J,
                    Blocks.N,
                    Blocks.I,
                    Blocks.F,
                }),


            // 24
            new GameSpace(new[,]
                {
                    { false, false, true , true , true , false },	//     ██████  
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { false, true , true , true , true , false },	//   ████████  
                },
                new List<Block>
                {
                    Blocks.I,
                    Blocks.C,
                    Blocks.L,
                    Blocks.Q,
                    Blocks.G,
                    Blocks.F,
                    Blocks.J,
                }),


            // 25
            new GameSpace(new[,]
                {
                    { true , true , true , true , true , true , true , true  },	// ████████████████
                    { true , true , true , true , true , true , true , true  },	// ████████████████
                    { true , true , true , true , true , true , true , true  },	// ████████████████
                    { true , true , true , true , true , true , true , true  },	// ████████████████
                },
                new List<Block>
                {
                    Blocks.M,
                    Blocks.S,
                    Blocks.Q,
                    Blocks.G,
                    Blocks.J,
                    Blocks.G,
                    Blocks.E,
                }),

            // 26
            new GameSpace(new[,]
                {
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { false, true , true , true , true , true , true  },	//   ████████████
                    { false, true , true , true , true , true , true  },	//   ████████████
                    { false, false, false, false, true , false, false },	//         ██    
                },
                new List<Block>
                {
                    Blocks.I,
                    Blocks.B,
                    Blocks.N,
                    Blocks.K,
                    Blocks.S,
                    Blocks.G,
                    Blocks.B,
                }),

            // 27
            new GameSpace(new[,]
                {
                    { false, false, true , true , true , false, false },	//     ██████    
                    { false, true , true , true , true , true , false },	//   ██████████  
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                },
                new List<Block>
                {
                    Blocks.O,
                    Blocks.O,
                    Blocks.K,
                    Blocks.E,
                    Blocks.G,
                    Blocks.G,
                    Blocks.K,
                }),

            // 28
            new GameSpace(new[,]
                {
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , false, true , false, true  },	// ████  ██  ██
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                },
                new List<Block>
                {
                    Blocks.C,
                    Blocks.I,
                    Blocks.G,
                    Blocks.B,
                    Blocks.H,
                    Blocks.F,
                    Blocks.K,
                }),

            // 29
            new GameSpace(new[,]
                {
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { false, true , true , false, true , true , true  },	//   ████  ██████
                    { false, true , true , true , true , true , true  },	//   ████████████
                    { false, true , true , true , true , true , true  },	//   ████████████
                },
                new List<Block>
                {
                    Blocks.D,
                    Blocks.L,
                    Blocks.H,
                    Blocks.S,
                    Blocks.L,
                    Blocks.J,
                    Blocks.N,
                }),

            // 30
            new GameSpace(new[,]
                {
                    { false, false, false, false, true , true , true  },	//         ██████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                },
                new List<Block>
                {
                    Blocks.P,
                    Blocks.I,
                    Blocks.C,
                    Blocks.Q,
                    Blocks.G,
                    Blocks.K,
                    Blocks.D,
                }),

            // 31
            new GameSpace(new[,]
                {
                    { false, false, true , true , true , true , true  },	//     ██████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { false, false, true , true , true , true , true  },	//     ██████████
                },
                new List<Block>
                {
                    Blocks.M,
                    Blocks.J,
                    Blocks.J,
                    Blocks.S,
                    Blocks.E,
                    Blocks.L,
                    Blocks.G,
                }),

            // 32
            new GameSpace(new[,]
                {
                    { true , true , true , true , false, false },	// ████████    
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                },
                new List<Block>
                {
                    Blocks.C,
                    Blocks.B,
                    Blocks.D,
                    Blocks.G,
                    Blocks.F,
                    Blocks.B,
                    Blocks.K,
                }),


            // 33
            new GameSpace(new[,]
                {
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { false, false, true , true , true , false, false },	//     ██████    
                },
                new List<Block>
                {
                    Blocks.Q,
                    Blocks.P,
                    Blocks.Q,
                    Blocks.D,
                    Blocks.G,
                    Blocks.F,
                    Blocks.A,
                }),

            // 34
            new GameSpace(new[,]
                {
                    { true , false, true , true , true , true , false },	// ██  ████████  
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { false, true , true , true , false, false, false },	//   ██████      
                },
                new List<Block>
                {
                    Blocks.M,
                    Blocks.L,
                    Blocks.E,
                    Blocks.A,
                    Blocks.R,
                    Blocks.L,
                    Blocks.I,
                }),

            // 35
            new GameSpace(new[,]
                {
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                },
                new List<Block>
                {
                    Blocks.N,
                    Blocks.Q,
                    Blocks.E,
                    Blocks.E,
                    Blocks.H,
                    Blocks.G,
                    Blocks.A,
                }),

            // 36
            new GameSpace(new[,]
                {
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { false, true , true , true , true , false, false },	//   ████████    
                },
                new List<Block>
                {
                    Blocks.D,
                    Blocks.N,
                    Blocks.Q,
                    Blocks.S,
                    Blocks.F,
                    Blocks.E,
                    Blocks.A,
                }),

            // 37
            new GameSpace(new[,]
                {
                    { false, true , true , true , true , true , false },	//   ██████████  
                    { false, true , true , true , true , true , true  },	//   ████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , false },	// ████████████  
                },
                new List<Block>
                {
                    Blocks.C,
                    Blocks.I,
                    Blocks.K,
                    Blocks.L,
                    Blocks.S,
                    Blocks.O,
                    Blocks.D,
                }),

            // 38
            new GameSpace(new[,]
                {
                    { true , true , true , false, false, false },	// ██████      
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                },
                new List<Block>
                {
                    Blocks.H,
                    Blocks.N,
                    Blocks.E,
                    Blocks.I,
                    Blocks.B,
                    Blocks.K,
                    Blocks.B,
                }),

            // 39
            new GameSpace(new[,]
                {
                    { true , true , true , true , true , false },	// ██████████  
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                },
                new List<Block>
                {
                    Blocks.O,
                    Blocks.N,
                    Blocks.Q,
                    Blocks.G,
                    Blocks.H,
                    Blocks.B,
                    Blocks.K,
                }),

            // 40
            new GameSpace(new[,]
                {
                    { false, false, false, false, true , false },	//         ██  
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { false, true , true , true , true , false },	//   ████████  
                },
                new List<Block>
                {
                    Blocks.J,
                    Blocks.Q,
                    Blocks.G,
                    Blocks.L,
                    Blocks.D,
                    Blocks.E,
                    Blocks.A,
                }),


            // 41
            new GameSpace(new[,]
                {
                    { false, false, false, true , false, false, false },	//       ██      
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                },
                new List<Block>
                {
                    Blocks.C,
                    Blocks.G,
                    Blocks.I,
                    Blocks.F,
                    Blocks.Q,
                    Blocks.G,
                    Blocks.A,
                }),

            // 42
            new GameSpace(new[,]
                {
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , false },	// ██████████  
                    { true , true , true , true , true , false },	// ██████████  
                },
                new List<Block>
                {
                    Blocks.J,
                    Blocks.L,
                    Blocks.L,
                    Blocks.M,
                    Blocks.Q,
                    Blocks.G,
                    Blocks.A,
                }),

            // 43
            new GameSpace(new[,]
                {
                    { false, true , true , true , true , true , false },	//   ██████████  
                    { false, true , true , true , true , true , true  },	//   ████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , false },	// ████████████  
                    { false, false, false, true , false, false, false },	//       ██      
                },
                new List<Block>
                {
                    Blocks.P,
                    Blocks.J,
                    Blocks.L,
                    Blocks.N,
                    Blocks.D,
                    Blocks.R,
                    Blocks.J,
                }),


            // 44
            new GameSpace(new[,]
                {
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                },
                new List<Block>
                {
                    Blocks.I,
                    Blocks.K,
                    Blocks.I,
                    Blocks.N,
                    Blocks.L,
                    Blocks.C,
                    Blocks.A,
                    Blocks.B,
                }),


            // 45
            new GameSpace(new[,]
                {
                    { false, true , true , true , true , false },	//   ████████  
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { false, true , true , true , true , false },	//   ████████  
                },
                new List<Block>
                {
                    Blocks.O,
                    Blocks.B,
                    Blocks.E,
                    Blocks.K,
                    Blocks.S,
                    Blocks.A,
                    Blocks.K,
                }),


            // 46
            new GameSpace(new[,]
                {
                    { false, true , true , true , true , true , true  },	//   ████████████
                    { false, true , true , true , true , true , true  },	//   ████████████
                    { false, true , true , true , true , true , true  },	//   ████████████
                    { true , true , true , true , true , true , false },	// ████████████  
                    { true , true , true , true , true , true , false },	// ████████████  
                },
                new List<Block>
                {
                    Blocks.I,
                    Blocks.D,
                    Blocks.J,
                    Blocks.Q,
                    Blocks.L,
                    Blocks.G,
                    Blocks.G,
                }),

            // 47
            new GameSpace(new[,]
                {
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , false },	// ██████████  
                },
                new List<Block>
                {
                    Blocks.I,
                    Blocks.N,
                    Blocks.J,
                    Blocks.G,
                    Blocks.I,
                    Blocks.B,
                    Blocks.F,
                }),

            // 48
            new GameSpace(new[,]
                {
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { false, true , true , true , true , true  },	//   ██████████
                },
                new List<Block>
                {
                    Blocks.I,
                    Blocks.K,
                    Blocks.G,
                    Blocks.J,
                    Blocks.B,
                    Blocks.G,
                    Blocks.D,
                    Blocks.A,
                }),










        };

        public static GameSpace GetTask(int index)
        {
            return tasks[index];
        }
    }
}
