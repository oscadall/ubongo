﻿using System;
using Ubongo.Enums;
using Ubongo.ViewModel;

namespace Ubongo.Model
{
    public class LevelState : PropertyChangedNotificator, IDisposable
    {

        private GamePosition gPosition;
        public LevelState(int level, Difficulty difficulty, GamePosition gamePosition)
        {
            Level = level;
            Difficulty = difficulty;
            gPosition = gamePosition;
            IsOpen = gamePosition.IsLevelOpen(Level,difficulty);
            IsSolved = gamePosition.IsLevelSolved(Level,difficulty);
            gamePosition.PropertyChanged += GamePosition_PropertyChanged;
        }

        public void Dispose()
        {
            gPosition.PropertyChanged -= GamePosition_PropertyChanged;
        }

        private void GamePosition_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var gamePosition = (GamePosition)sender;
            IsOpen = gamePosition.IsLevelOpen(Level, Difficulty);
            IsSolved = gamePosition.IsLevelSolved(Level, Difficulty);
        }

        public int Level { get; set; }

        public bool IsOpen { get; set; }

        public bool IsSolved { get; set; }

        public Difficulty Difficulty { get; set; }
    }


}
