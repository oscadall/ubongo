﻿using System.Collections.Generic;

namespace Ubongo.Model
{
    public class MiddleTasks
    {
        private static GameSpace[] tasks = new GameSpace[]
        {
            // 1
            new GameSpace(new[,]
                {
                    { false, true , true , true , true , false },	//   ████████  
                    { true , true , true , true , true , false },	// ██████████  
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { false, true , true , true , false, false },	//   ██████    
                },
                new List<Block>
                {
                    Blocks.L,
                    Blocks.M,
                    Blocks.L,
                    Blocks.L,
                    Blocks.G,
                    Blocks.B,
                }),

            // 1
            new GameSpace(new bool[,]
                {
                    { false, false, true , false, false, true , false, false },	//     ██    ██    
                    { true , true , true , true , true , true , false, false },	// ████████████    
                    { true , true , true , true , true , true , false, false },	// ████████████    
                    { false, true , true , true , true , true , true , true  },	//   ██████████████
                    { false, false, false, true , true , true , true , false },	//       ████████  
                },
                new List<Block>
                {
                    Blocks.G,
                    Blocks.J,
                    Blocks.E,
                    Blocks.L,
                    Blocks.N,
                    Blocks.K,
                }),

            // 2
            new GameSpace(new bool[,]
                {
                    { false, false, true , true , true , false },	//     ██████  
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { false, true , true , true , false, false },	//   ██████    
                },
                new List<Block>
                {
                    Blocks.B,
                    Blocks.C,
                    Blocks.G,
                    Blocks.H,
                    Blocks.J,
                    Blocks.L,
                }),

            // 3
            new GameSpace(new bool[,]
                {
                    { false, false, false, false, true , true , true  },	//         ██████
                    { false, true , true , true , true , true , true  },	//   ████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { false, true , true , true , true , true , true  },	//   ████████████
                    { false, false, true , true , true , false, false },	//     ██████    
                },
                new List<Block>
                {
                    Blocks.M,
                    Blocks.I,
                    Blocks.L,
                    Blocks.B,
                    Blocks.N,
                    Blocks.G,
                }),

            // 4
            new GameSpace(new bool[,]
                {
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , false },	// ██████████  
                },
                new List<Block>
                {
                    Blocks.K,
                    Blocks.D,
                    Blocks.G,
                    Blocks.F,
                    Blocks.A,
                    Blocks.G,
                }),

            // 5
            new GameSpace(new bool[,]
                {
                    { true , true , true , true , true , false },	// ██████████  
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , false },	// ██████████  
                    { true , true , true , true , true , false },	// ██████████  
                    { false, true , true , true , true , false },	//   ████████  
                },
                new List<Block>
                {
                    Blocks.L,
                    Blocks.M,
                    Blocks.J,
                    Blocks.D,
                    Blocks.K,
                    Blocks.J,
                }),

            // 6
            new GameSpace(new bool[,]
                {
                    { true , false, true , true , true , false, false },	// ██  ██████    
                    { true , true , true , true , true , true , false },	// ████████████  
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , false },	// ████████████  
                    { false, false, false, false, true , true , false },	//         ████  
                },
                new List<Block>
                {
                    Blocks.R,
                    Blocks.I,
                    Blocks.J,
                    Blocks.B,
                    Blocks.D,
                    Blocks.H,
                }),


            // 7
            new GameSpace(new bool[,]
                {
                    { false, true , false, false, false, false },	//   ██        
                    { true , true , true , false, true , true  },	// ██████  ████
                    { true , true , true , true , true , true  },	// ████████████
                    { false, true , false, true , true , true  },	//   ██  ██████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                },
                new List<Block>
                {
                    Blocks.O,
                    Blocks.L,
                    Blocks.Q,
                    Blocks.J,
                    Blocks.N,
                    Blocks.F,
                }),

            // 8
            new GameSpace(new bool[,]
                {
                    { false, true , true , true , true , true , false },	//   ██████████  
                    { true , true , true , true , true , true , false },	// ████████████  
                    { true , true , true , true , false, true , true  },	// ████████  ████
                    { false, true , true , true , true , true , true  },	//   ████████████
                    { false, true , true , true , true , true , false },	//   ██████████  
                },
                new List<Block>
                {
                    Blocks.R,
                    Blocks.P,
                    Blocks.E,
                    Blocks.L,
                    Blocks.D,
                    Blocks.H,
                }),

            // 9
            new GameSpace(new bool[,]
                {
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , false, true , true , true  },	// ████  ██████
                    { false, true , true , true , true , true  },	//   ██████████
                },
                new List<Block>
                {
                    Blocks.K,
                    Blocks.K,
                    Blocks.K,
                    Blocks.J,
                    Blocks.E,
                    Blocks.G,
                }),

            // 10
            new GameSpace(new bool[,]
                {
                    { false, true , true , true , true , true , true  },	//   ████████████
                    { false, true , true , true , true , true , true  },	//   ████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                },
                new List<Block>
                {
                    Blocks.C,
                    Blocks.O,
                    Blocks.N,
                    Blocks.G,
                    Blocks.J,
                    Blocks.K,
                }),

            // 11
            new GameSpace(new bool[,]
                {
                    { false, false, false, true , false, false, false },	//       ██      
                    { false, true , true , true , true , true , false },	//   ██████████  
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , false, true , true , true , true , true  },	// ██  ██████████
                },
                new List<Block>
                {
                    Blocks.Q,
                    Blocks.J,
                    Blocks.E,
                    Blocks.L,
                    Blocks.I,
                    Blocks.G,
                }),

            // 12
            new GameSpace(new bool[,]
                {
                    { false, true , true , true , true , false },	//   ████████  
                    { true , true , true , false, true , true  },	// ██████  ████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , false, true , true  },	// ██████  ████
                    { true , true , true , true , true , true  },	// ████████████
                },
                new List<Block>
                {
                    Blocks.E,
                    Blocks.F,
                    Blocks.B,
                    Blocks.Q,
                    Blocks.H,
                    Blocks.L,
                }),

            // 13
            new GameSpace(new bool[,]
                {
                    { false, false, true , true , true , true , true , true , false },	//     ████████████  
                    { false, true , true , true , true , true , true , true , true  },	//   ████████████████
                    { true , true , true , true , true , true , true , true , false },	// ████████████████  
                    { true , true , true , true , true , true , false, false, false },	// ████████████      
                },
                new List<Block>
                {
                    Blocks.N,
                    Blocks.D,
                    Blocks.L,
                    Blocks.N,
                    Blocks.J,
                    Blocks.M,
                }),

            // 14
            new GameSpace(new bool[,]
                {
                    { true , true , false, false, false, false, false },	// ████          
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { false, true , true , true , true , true , true  },	//   ████████████
                    { false, false, false, true , true , false, false },	//       ████    
                },
                new List<Block>
                {
                    Blocks.J,
                    Blocks.A,
                    Blocks.F,
                    Blocks.N,
                    Blocks.L,
                    Blocks.G,
                }),

            // 15
            new GameSpace(new bool[,]
                {
                    { true , true , true , true , true , true , false },	// ████████████  
                    { false, true , true , true , true , false, false },	//   ████████    
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , false, true , true , true , true  },	// ████  ████████
                },
                new List<Block>
                {
                    Blocks.D,
                    Blocks.C,
                    Blocks.M,
                    Blocks.R,
                    Blocks.F,
                    Blocks.O,
                }),

            // 16
            new GameSpace(new bool[,]
                {
                    { false, false, false, true , false, false, false },	//       ██      
                    { true , true , true , true , true , true , false },	// ████████████  
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , false, true , true , true , true  },	// ████  ████████
                    { true , true , true , true , false, true , true  },	// ████████  ████
                },
                new List<Block>
                {
                    Blocks.E,
                    Blocks.E,
                    Blocks.G,
                    Blocks.J,
                    Blocks.E,
                    Blocks.K,
                }),

            // 17
            new GameSpace(new bool[,]
                {
                    { true , true , true , true , false, false, false },	// ████████      
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { false, true , true , true , true , true , true  },	//   ████████████
                    { false, true , true , true , true , true , false },	//   ██████████  
                },
                new List<Block>
                {
                    Blocks.Q,
                    Blocks.Q,
                    Blocks.N,
                    Blocks.E,
                    Blocks.N,
                    Blocks.I,
                }),


            // 18
            new GameSpace(new bool[,]
                {
                    { false, false, true , false, false, false, false },	//     ██        
                    { false, true , true , true , true , true , false },	//   ██████████  
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { false, true , true , true , true , true , false },	//   ██████████  
                },
                new List<Block>
                {
                    Blocks.R,
                    Blocks.L,
                    Blocks.L,
                    Blocks.L,
                    Blocks.L,
                    Blocks.L,
                }),

           
            // 19
            new GameSpace(new bool[,]
                {
                    { false, true , true , true , true , true , true  },	//   ████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , false },	// ████████████  
                    { true , true , true , true , false, false, false },	// ████████      
                    { false, true , true , true , false, false, false },	//   ██████      
                },
                new List<Block>
                {
                    Blocks.A,
                    Blocks.E,
                    Blocks.D,
                    Blocks.O,
                    Blocks.J,
                    Blocks.R,
                }),

            // 20
            new GameSpace(new bool[,]
                {
                    { true , false, false, true , true , false },	// ██    ████  
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                },
                new List<Block>
                {
                    Blocks.F,
                    Blocks.I,
                    Blocks.L,
                    Blocks.D,
                    Blocks.L,
                    Blocks.E,
                }),

            // 21
            new GameSpace(new bool[,]
                {
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , false, true , true  },	// ██████  ████
                    { true , true , true , true , true , true  },	// ████████████
                    { false, false, true , true , true , false },	//     ██████  
                },
                new List<Block>
                {
                    Blocks.P,
                    Blocks.J,
                    Blocks.J,
                    Blocks.D,
                    Blocks.G,
                    Blocks.I,
                }),

            // 22
            new GameSpace(new bool[,]
                {
                    { false, true , true , true , false, false },	//   ██████    
                    { true , true , true , true , true , false },	// ██████████  
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , false, true , true , true , true  },	// ██  ████████
                },
                new List<Block>
                {
                    Blocks.C,
                    Blocks.G,
                    Blocks.I,
                    Blocks.L,
                    Blocks.J,
                    Blocks.I,
                }),

            // 23
            new GameSpace(new bool[,]
                {
                    { true , true , true , true , true , false },	// ██████████  
                    { true , true , true , true , true , false },	// ██████████  
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                },
                new List<Block>
                {
                    Blocks.E,
                    Blocks.R,
                    Blocks.L,
                    Blocks.P,
                    Blocks.J,
                    Blocks.F,
                }),

            // 24
            new GameSpace(new bool[,]
                {
                    { false, true , true , true , true , false, false },	//   ████████    
                    { false, true , true , true , true , true , true  },	//   ████████████
                    { false, true , true , true , true , true , true  },	//   ████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { false, false, true , true , true , false, false },	//     ██████    
                },
                new List<Block>
                {
                    Blocks.J,
                    Blocks.E,
                    Blocks.L,
                    Blocks.N,
                    Blocks.G,
                    Blocks.G,
                }),

            // 25
            new GameSpace(new bool[,]
                {
                    { false, true , true , true , true , true , false },	//   ██████████  
                    { false, true , true , true , true , true , false },	//   ██████████  
                    { true , true , false, true , true , true , true  },	// ████  ████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                },
                new List<Block>
                {
                    Blocks.R,
                    Blocks.F,
                    Blocks.A,
                    Blocks.C,
                    Blocks.H,
                    Blocks.P,
                    Blocks.J,
                }),

            // 26
            new GameSpace(new[,]
                {
                    { true , true , true , true , true , true , true , false },	// ██████████████  
                    { true , true , true , false, true , true , true , false },	// ██████  ██████  
                    { true , true , true , false, true , false, true , true  },	// ██████  ██  ████
                    { true , true , true , true , true , true , true , true  },	// ████████████████
                },
                new List<Block>
                {
                    Blocks.D,
                    Blocks.G,
                    Blocks.G,
                    Blocks.D,
                    Blocks.J,
                    Blocks.E,
                }),

            // 27
            new GameSpace(new[,]
                {
                    { false, true , true , true , false, false },	//   ██████    
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , false },	// ██████████  
                    { true , true , false, false, false, false },	// ████        
                },
                new List<Block>
                {
                    Blocks.K,
                    Blocks.Q,
                    Blocks.Q,
                    Blocks.K,
                    Blocks.B,
                    Blocks.B,
                }),

            // 28
            new GameSpace(new[,]
                {
                    { false, true , true , true , true , false },	//   ████████  
                    { false, true , true , true , true , true  },	//   ██████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                },
                new List<Block>
                {
                    Blocks.J,
                    Blocks.M,
                    Blocks.E,
                    Blocks.L,
                    Blocks.N,
                    Blocks.G,
                }),

            // 29
            new GameSpace(new[,]
                {
                    { true , true , false, true , true , true  },	// ████  ██████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , false, true , true , true , true  },	// ██  ████████
                    { true , true , true , true , true , true  },	// ████████████
                },
                new List<Block>
                {
                    Blocks.C,
                    Blocks.H,
                    Blocks.J,
                    Blocks.A,
                    Blocks.L,
                    Blocks.B,
                }),

            // 30
            new GameSpace(new[,]
                {
                    { false, true , false, false, true , false, false, false },	//   ██    ██      
                    { true , true , true , true , true , false, true , false },	// ██████████  ██  
                    { false, true , true , true , true , true , true , true  },	//   ██████████████
                    { false, true , true , true , true , true , true , true  },	//   ██████████████
                    { false, true , true , true , true , false, true , false },	//   ████████  ██  
                    { false, false, true , false, false, false, false, false },	//     ██          
                },
                new List<Block>
                {
                    Blocks.F,
                    Blocks.C,
                    Blocks.L,
                    Blocks.J,
                    Blocks.M,
                    Blocks.E,
                }),

            // 31
            new GameSpace(new[,]
                {
                    { false, true , true , true , true , false },	//   ████████  
                    { true , true , true , true , true , false },	// ██████████  
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , false },	// ██████████  
                },
                new List<Block>
                {
                    Blocks.I,
                    Blocks.O,
                    Blocks.J,
                    Blocks.R,
                    Blocks.Q,
                    Blocks.B,
                }),

            // 32
            new GameSpace(new[,]
                {
                    { false, true , true , true , true , false, false },	//   ████████    
                    { true , false, true , true , true , true , false },	// ██  ████████  
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { false, true , true , true , true , true , true  },	//   ████████████
                },
                new List<Block>
                {
                    Blocks.D,
                    Blocks.C,
                    Blocks.J,
                    Blocks.E,
                    Blocks.D,
                    Blocks.Q,
                }),

            // 33
            new GameSpace(new[,]
                {
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { false, false, true , false, false, true , true  },	//     ██    ████
                },
                new List<Block>
                {
                    Blocks.P,
                    Blocks.N,
                    Blocks.G,
                    Blocks.K,
                    Blocks.J,
                    Blocks.K,
                }),

            // 34
            new GameSpace(new[,]
                {
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { false, true , true , true , true , true , true  },	//   ████████████
                },
                new List<Block>
                {
                    Blocks.J,
                    Blocks.J,
                    Blocks.N,
                    Blocks.Q,
                    Blocks.Q,
                    Blocks.H,
                }),

            // 35
            new GameSpace(new[,]
                {
                    { false, false, true , true , false, false, false },	//     ████      
                    { false, true , true , true , true , true , false },	//   ██████████  
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , false },	// ████████████  
                },
                new List<Block>
                {
                    Blocks.M,
                    Blocks.L,
                    Blocks.G,
                    Blocks.R,
                    Blocks.L,
                    Blocks.D,
                }),

            // 36
            new GameSpace(new[,]
                {
                    { false, true , true , true , false, false },	//   ██████    
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                },
                new List<Block>
                {
                    Blocks.R,
                    Blocks.M,
                    Blocks.L,
                    Blocks.B,
                    Blocks.Q,
                    Blocks.Q,
                }),

// 37
            new GameSpace(new[,]
                {
                    { true , true , true , false, false, false, false },	// ██████        
                    { true , true , true , false, true , false, false },	// ██████  ██    
                    { true , true , true , true , true , true , false },	// ████████████  
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , false },	// ████████████  
                },
                new List<Block>
                {
                    Blocks.G,
                    Blocks.M,
                    Blocks.Q,
                    Blocks.E,
                    Blocks.H,
                    Blocks.K,
                }),

            // 38
            new GameSpace(new[,]
                {
                    { false, true , true , true , true  },	//   ████████
                    { true , true , true , true , true  },	// ██████████
                    { true , true , true , true , true  },	// ██████████
                    { true , true , true , true , true  },	// ██████████
                    { false, true , true , false, true  },	//   ████  ██
                },
                new List<Block>
                {
                    Blocks.R,
                    Blocks.P,
                    Blocks.N,
                    Blocks.I,
                    Blocks.B,
                }),

            // 39
            new GameSpace(new[,]
                {
                    { true , false, false, false, false },	// ██        
                    { true , true , true , true , true  },	// ██████████
                    { true , true , true , true , true  },	// ██████████
                    { true , true , true , true , true  },	// ██████████
                    { true , true , true , true , true  },	// ██████████
                    { true , true , true , true , true  },	// ██████████
                },
                new List<Block>
                {
                    Blocks.O,
                    Blocks.I,
                    Blocks.J,
                    Blocks.K,
                    Blocks.F,
                    Blocks.D,
                }),

            // 40
            new GameSpace(new[,]
                {
                    { false, false, true , true , true , true , false },	//     ████████  
                    { false, true , true , true , true , true , true  },	//   ████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { false, true , true , true , true , true , false },	//   ██████████  
                    { false, true , true , true , true , true , true  },	//   ████████████
                },
                new List<Block>
                {
                    Blocks.C,
                    Blocks.E,
                    Blocks.R,
                    Blocks.M,
                    Blocks.L,
                    Blocks.G,
                }),

            // 41
            new GameSpace(new[,]
                {
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                },
                new List<Block>
                {
                    Blocks.G,
                    Blocks.H,
                    Blocks.B,
                    Blocks.I,
                    Blocks.L,
                    Blocks.E,
                }),

            // 42
            new GameSpace(new[,]
                {
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { false, true , true , true , true , true  },	//   ██████████
                },
                new List<Block>
                {
                    Blocks.O,
                    Blocks.Q,
                    Blocks.J,
                    Blocks.A,
                    Blocks.G,
                    Blocks.K,
                }),

            // 43
            new GameSpace(new[,]
                {
                    { true , true , true , true , true , true , true , false },	// ██████████████  
                    { true , true , true , true , true , true , true , false },	// ██████████████  
                    { true , true , true , true , true , true , true , false },	// ██████████████  
                    { true , true , true , false, true , true , true , true  },	// ██████  ████████
                },
                new List<Block>
                {
                    Blocks.N,
                    Blocks.Q,
                    Blocks.H,
                    Blocks.I,
                    Blocks.D,
                    Blocks.F,
                }),

            // 44
            new GameSpace(new[,]
                {
                    { false, false, true , true , false, false },	//     ████    
                    { true , true , true , true , true , false },	// ██████████  
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                },
                new List<Block>
                {
                    Blocks.K,
                    Blocks.J,
                    Blocks.E,
                    Blocks.M,
                    Blocks.B,
                    Blocks.D,
                }),

            // 45
            new GameSpace(new[,]
                {
                    { false, false, true , true , false, false },	//     ████    
                    { true , true , true , true , true , false },	// ██████████  
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                },
                new List<Block>
                {
                    Blocks.K,
                    Blocks.J,
                    Blocks.E,
                    Blocks.M,
                    Blocks.B,
                    Blocks.D,
                }),

            // 46
            new GameSpace(new[,]
                {
                    { false, true , true , true , true , false },	//   ████████  
                    { false, true , true , true , true , false },	//   ████████  
                    { true , true , true , true , true , false },	// ██████████  
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , false },	// ██████████  
                },
                new List<Block>
                {
                    Blocks.D,
                    Blocks.L,
                    Blocks.I,
                    Blocks.A,
                    Blocks.C,
                    Blocks.G,
                }),

            // 47
            new GameSpace(new[,]
                {
                    { true , true , true , true , true , true , false },	// ████████████  
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                },
                new List<Block>
                {
                    Blocks.N,
                    Blocks.M,
                    Blocks.Q,
                    Blocks.E,
                    Blocks.D,
                    Blocks.A,
                }),

            // 48
            new GameSpace(new[,]
                {
                    { true , true , true , true , false, false },	// ████████    
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , false, false, false },	// ██████      
                    { false, true , false, false, false, false },	//   ██        
                },
                new List<Block>
                {
                    Blocks.C,
                    Blocks.R,
                    Blocks.B,
                    Blocks.I,
                    Blocks.H,
                    Blocks.N,
                }),










        };

        public static GameSpace GetTask(int index)
        {
            return tasks[index];
        }
    }
}
