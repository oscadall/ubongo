﻿using System.Collections.Generic;

namespace Ubongo.Model
{
    public class DevilTasks
    {
        private static GameSpace[] tasks = new GameSpace[]
        {
            // 1
            new GameSpace(new[,]
                {
                    { false, false, true , false, false, false, false, false, false },	//     ██            
                    { true , true , true , true , true , true , true , true , false },	// ████████████████  
                    { true , true , true , true , true , true , true , true , false },	// ████████████████  
                    { true , true , true , true , true , true , true , true , true  },	// ██████████████████
                    { true , true , true , true , true , true , true , true , true  },	// ██████████████████
                    { false, true , true , true , true , true , true , true , false },	//   ██████████████  
                    { false, true , true , true , true , true , true , false, false },	//   ████████████    
                },
                new List<Block>
                {
                    Blocks.I,
                    Blocks.C,
                    Blocks.J,
                    Blocks.C,
                    Blocks.M,
                    Blocks.E,
                    Blocks.N,
                    Blocks.Q,
                    Blocks.D,
                    Blocks.R,
                }),

            // 2
            new GameSpace(new[,]
                {
                    { true , true , true , true , true , true , true , false },	// ██████████████  
                    { true , true , true , true , true , true , true , true  },	// ████████████████
                    { true , true , true , true , true , true , true , true  },	// ████████████████
                    { true , true , true , true , true , true , true , true  },	// ████████████████
                    { true , true , true , true , true , true , true , true  },	// ████████████████
                    { true , true , true , true , true , false, false, false },	// ██████████      
                },
                new List<Block>
                {
                    Blocks.M,
                    Blocks.L,
                    Blocks.O,
                    Blocks.J,
                    Blocks.H,
                    Blocks.R,
                    Blocks.B,
                    Blocks.C,
                    Blocks.D,
                    Blocks.G,
                }),

            // 3
            new GameSpace(new[,]
                {
                    { true , true , true , true , true , true , true , false },	// ██████████████  
                    { true , true , true , true , true , true , true , false },	// ██████████████  
                    { true , true , true , true , true , true , true , true  },	// ████████████████
                    { true , true , true , true , true , true , true , true  },	// ████████████████
                    { true , true , true , true , true , true , true , false },	// ██████████████  
                    { false, true , true , true , true , true , true , false },	//   ████████████  
                },
                new List<Block>
                {
                    Blocks.H,
                    Blocks.P,
                    Blocks.Q,
                    Blocks.N,
                    Blocks.J,
                    Blocks.B,
                    Blocks.J,
                    Blocks.E,
                    Blocks.C,
                    Blocks.B,
                }),

            // 4
            new GameSpace(new[,]
                {
                    { false, true , false, true , true , true , true , true , true  },	//   ██  ████████████
                    { true , true , true , true , true , true , true , true , true  },	// ██████████████████
                    { true , true , true , true , true , true , true , true , true  },	// ██████████████████
                    { true , true , true , true , true , true , true , true , true  },	// ██████████████████
                    { true , true , true , true , true , true , true , true , true  },	// ██████████████████
                    { false, false, false, true , true , true , true , true , false },	//       ██████████  
                },
                new List<Block>
                {
                    Blocks.R,
                    Blocks.L,
                    Blocks.G,
                    Blocks.Q,
                    Blocks.E,
                    Blocks.N,
                    Blocks.Q,
                    Blocks.F,
                    Blocks.E,
                    Blocks.N,
                }),

            // 5
            new GameSpace(new[,]
                {
                    { true , true , true , true , true , false, true , false },	// ██████████  ██  
                    { true , true , true , true , true , true , true , true  },	// ████████████████
                    { true , true , true , true , true , true , true , true  },	// ████████████████
                    { true , true , true , true , true , true , true , true  },	// ████████████████
                    { true , true , true , true , true , true , true , true  },	// ████████████████
                    { true , true , true , true , true , true , true , true  },	// ████████████████
                },
                new List<Block>
                {
                    Blocks.P,
                    Blocks.N,
                    Blocks.F,
                    Blocks.Q,
                    Blocks.L,
                    Blocks.E,
                    Blocks.G,
                    Blocks.G,
                    Blocks.D,
                    Blocks.H,
                }),


            //// 6
            new GameSpace(new[,]
                {
                    { false, true , true , true , true , true , true , false, true , true , false, true  },	//   ████████████  ████  ██
                    { true , true , true , true , true , true , true , true , true , true , true , true  },	// ████████████████████████
                    { true , true , true , true , true , true , true , true , true , true , true , true  },	// ████████████████████████
                    { true , true , true , true , true , true , true , true , true , true , true , true  },	// ████████████████████████
                    { true , true , true , true , true , true , true , true , true , true , true , true  },	// ████████████████████████
                    { true , true , true , true , true , true , true , true , true , true , true , true  },	// ████████████████████████
                    { true , true , true , true , true , true , true , true , true , true , true , true  },	// ████████████████████████
                    { true , true , true , false, true , true , true , false, true , false, false, false },	// ██████  ██████  ██      
                },
                new List<Block>
                {
                    Blocks.S,
                    Blocks.C,
                    Blocks.G,
                    Blocks.M,
                    Blocks.D,
                    Blocks.E,
                    Blocks.P,
                    Blocks.H,
                    Blocks.N,
                    Blocks.K,
                    Blocks.R,
                    Blocks.L,
                    Blocks.G,
                    Blocks.O,
                    Blocks.Q,
                    Blocks.J,
                    Blocks.P,
                    Blocks.S,
                    Blocks.D,
                }),

        };

        public static GameSpace GetTask(int index)
        {
            return tasks[index];
        }
    }
}
