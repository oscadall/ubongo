﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ubongo.Model
{
    public class ExpertTasks
    {
        private static GameSpace[] tasks = new GameSpace[]
        {
            // 1
            new GameSpace(new bool[,]
                {
                    { false, false, true , false, false, true , false, false },	//     ██    ██    
                    { true , true , true , true , true , true , false, false },	// ████████████    
                    { true , true , true , true , true , true , false, false },	// ████████████    
                    { false, true , true , true , true , true , true , true  },	//   ██████████████
                    { false, false, false, true , true , true , true , false },	//       ████████  
                },
                new List<Block>
                {
                    Blocks.G,
                    Blocks.J,
                    Blocks.E,
                    Blocks.L,
                    Blocks.N,
                    Blocks.K,
                }),

            // 2
            new GameSpace(new bool[,]
                {
                    { false, false, true , true , true , false },	//     ██████  
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { false, true , true , true , false, false },	//   ██████    
                },
                new List<Block>
                {
                    Blocks.B,
                    Blocks.C,
                    Blocks.G,
                    Blocks.H,
                    Blocks.J,
                    Blocks.L,
                }),

            // 3
            new GameSpace(new bool[,]
                {
                    { false, false, false, false, true , true , true  },	//         ██████
                    { false, true , true , true , true , true , true  },	//   ████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { false, true , true , true , true , true , true  },	//   ████████████
                    { false, false, true , true , true , false, false },	//     ██████    
                },
                new List<Block>
                {
                    Blocks.M,
                    Blocks.I,
                    Blocks.L,
                    Blocks.B,
                    Blocks.N,
                    Blocks.G,
                }),

            // 4
            new GameSpace(new bool[,]
                {
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , false },	// ██████████  
                },
                new List<Block>
                {
                    Blocks.K,
                    Blocks.D,
                    Blocks.G,
                    Blocks.F,
                    Blocks.A,
                    Blocks.G,
                }),

            // 5
            new GameSpace(new bool[,]
                {
                    { true , true , true , true , true , false },	// ██████████  
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , false },	// ██████████  
                    { true , true , true , true , true , false },	// ██████████  
                    { false, true , true , true , true , false },	//   ████████  
                },
                new List<Block>
                {
                    Blocks.L,
                    Blocks.M,
                    Blocks.J,
                    Blocks.D,
                    Blocks.K,
                    Blocks.J,
                }),

            // 6
            new GameSpace(new bool[,]
                {
                    { true , false, true , true , true , false, false },	// ██  ██████    
                    { true , true , true , true , true , true , false },	// ████████████  
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , false },	// ████████████  
                    { false, false, false, false, true , true , false },	//         ████  
                },
                new List<Block>
                {
                    Blocks.R,
                    Blocks.I,
                    Blocks.J,
                    Blocks.B,
                    Blocks.D,
                    Blocks.H,
                }),


            // 7
            new GameSpace(new bool[,]
                {
                    { false, true , false, false, false, false },	//   ██        
                    { true , true , true , false, true , true  },	// ██████  ████
                    { true , true , true , true , true , true  },	// ████████████
                    { false, true , false, true , true , true  },	//   ██  ██████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                },
                new List<Block>
                {
                    Blocks.O,
                    Blocks.L,
                    Blocks.Q,
                    Blocks.J,
                    Blocks.N,
                    Blocks.F,
                }),

            // 8
            new GameSpace(new bool[,]
                {
                    { false, true , true , true , true , true , false },	//   ██████████  
                    { true , true , true , true , true , true , false },	// ████████████  
                    { true , true , true , true , false, true , true  },	// ████████  ████
                    { false, true , true , true , true , true , true  },	//   ████████████
                    { false, true , true , true , true , true , false },	//   ██████████  
                },
                new List<Block>
                {
                    Blocks.R,
                    Blocks.P,
                    Blocks.E,
                    Blocks.L,
                    Blocks.D,
                    Blocks.H,
                }),

            // 9
            new GameSpace(new bool[,]
                {
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , false, true , true , true  },	// ████  ██████
                    { false, true , true , true , true , true  },	//   ██████████
                },
                new List<Block>
                {
                    Blocks.K,
                    Blocks.K,
                    Blocks.K,
                    Blocks.J,
                    Blocks.E,
                    Blocks.G,
                }),

            // 10
            new GameSpace(new bool[,]
                {
                    { false, true , true , true , true , true , true  },	//   ████████████
                    { false, true , true , true , true , true , true  },	//   ████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                },
                new List<Block>
                {
                    Blocks.C,
                    Blocks.O,
                    Blocks.N,
                    Blocks.G,
                    Blocks.J,
                    Blocks.K,
                }),

            // 11
            new GameSpace(new bool[,]
                {
                    { false, false, false, true , false, false, false },	//       ██      
                    { false, true , true , true , true , true , false },	//   ██████████  
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , false, true , true , true , true , true  },	// ██  ██████████
                },
                new List<Block>
                {
                    Blocks.Q,
                    Blocks.J,
                    Blocks.E,
                    Blocks.L,
                    Blocks.I,
                    Blocks.G,
                }),

            // 12
            new GameSpace(new bool[,]
                {
                    { false, true , true , true , true , false },	//   ████████  
                    { true , true , true , false, true , true  },	// ██████  ████
                    { true , true , true , true , true , true  },	// ████████████
                    { true , true , true , false, true , true  },	// ██████  ████
                    { true , true , true , true , true , true  },	// ████████████
                },
                new List<Block>
                {
                    Blocks.E,
                    Blocks.F,
                    Blocks.B,
                    Blocks.Q,
                    Blocks.H,
                    Blocks.L,
                }),

            // 13
            new GameSpace(new bool[,]
                {
                    { false, false, true , true , true , true , true , true , false },	//     ████████████  
                    { false, true , true , true , true , true , true , true , true  },	//   ████████████████
                    { true , true , true , true , true , true , true , true , false },	// ████████████████  
                    { true , true , true , true , true , true , false, false, false },	// ████████████      
                },
                new List<Block>
                {
                    Blocks.N,
                    Blocks.D,
                    Blocks.L,
                    Blocks.N,
                    Blocks.J,
                    Blocks.M,
                }),

            // 14
            new GameSpace(new bool[,]
                {
                    { true , true , false, false, false, false, false },	// ████          
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { false, true , true , true , true , true , true  },	//   ████████████
                    { false, false, false, true , true , false, false },	//       ████    
                },
                new List<Block>
                {
                    Blocks.J,
                    Blocks.A,
                    Blocks.F,
                    Blocks.N,
                    Blocks.L,
                    Blocks.G,
                }),

            // 15
            new GameSpace(new bool[,]
                {
                    { true , true , true , true , true , true , false },	// ████████████  
                    { false, true , true , true , true , false, false },	//   ████████    
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , false, true , true , true , true  },	// ████  ████████
                },
                new List<Block>
                {
                    Blocks.D,
                    Blocks.C,
                    Blocks.M,
                    Blocks.R,
                    Blocks.F,
                    Blocks.O,
                }),

            // 16
            new GameSpace(new bool[,]
                {
                    { false, false, false, true , false, false, false },	//       ██      
                    { true , true , true , true , true , true , false },	// ████████████  
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , false, true , true , true , true  },	// ████  ████████
                    { true , true , true , true , false, true , true  },	// ████████  ████
                },
                new List<Block>
                {
                    Blocks.E,
                    Blocks.E,
                    Blocks.G,
                    Blocks.J,
                    Blocks.E,
                    Blocks.K,
                }),

            // 17
            new GameSpace(new bool[,]
                {
                    { true , true , true , true , false, false, false },	// ████████      
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { false, true , true , true , true , true , true  },	//   ████████████
                    { false, true , true , true , true , true , false },	//   ██████████  
                },
                new List<Block>
                {
                    Blocks.Q,
                    Blocks.Q,
                    Blocks.N,
                    Blocks.E,
                    Blocks.N,
                    Blocks.I,
                }),


            // 18
            new GameSpace(new bool[,]
                {
                    { false, false, true , false, false, false, false },	//     ██        
                    { false, true , true , true , true , true , false },	//   ██████████  
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { true , true , true , true , true , true , true  },	// ██████████████
                    { false, true , true , true , true , true , false },	//   ██████████  
                },
                new List<Block>
                {
                    Blocks.R,
                    Blocks.L,
                    Blocks.L,
                    Blocks.L,
                    Blocks.L,
                    Blocks.L,
                }),










        };

        public static GameSpace GetTask(int index)
        {
            return tasks[index];
        }
    }
}
