﻿using System;

namespace Ubongo.Model
{
    public class Block
    {
        private static readonly Random Random = new Random(DateTime.Now.Millisecond);

        public Block(string name, bool[,] blockDefinition)
        {
            BlockDefinition = blockDefinition;
            Name = name;
        }

        public event Action PositionChanged;

        public string Name { get; }

        public bool[,] BlockDefinition { get; private set; }

        public bool this[int x, int y] => BlockDefinition[y, x];

        public int Width => BlockDefinition.GetLength(1);
        public int Height => BlockDefinition.GetLength(0);

        public int X { get; private set; } = -1;
        public int Y { get; private set; } = -1;

        public bool HasPositionInGameSpace => X != -1 && Y != -1;

        public void SetPosition(int x, int y)
        {
            X = x;
            Y = y;
            PositionChanged?.Invoke();
        }

        public void RemovePosition()
        {
            X = -1;
            Y = -1;
            PositionChanged?.Invoke();
        }

        public void Rotate()
        {
            var newBlockArray = new bool[Width, Height];
            for (int i = 0; i < Height; i++)
            {
                for (int j = 0; j < Width; j++)
                {
                    newBlockArray[Width - j - 1, i] = BlockDefinition[i, j];
                }
            }
            BlockDefinition =  newBlockArray;
        }

        public void Invert()
        {
            var w = BlockDefinition.GetLength(0);
            var h = BlockDefinition.GetLength(1);
            var newBlockArray = new bool[w, h];
            for (int i = 0; i < w; i++)
            {
                for (int j = 0; j < h; j++)
                {
                    newBlockArray[i, j] = BlockDefinition[i, h - 1 - j];
                }
            }

            BlockDefinition = newBlockArray;
        }


        public void RandomRotate()
        {
            var rotateCnt = Random.Next(4);
            var invert = Random.Next(2);

            for (int i = 0; i < rotateCnt; i++)
            {
                Rotate();
            }

            if (invert == 1)
                Invert();
        }

        public override string ToString()
        {
            var top = 0;
            var left = 0;
            var right = Width;
            var bottom = Height;
            var code = "";

            for (int i = top; i < bottom; i++)
            {
                code += "{ ";
                for (int j = left; j < right; j++)
                {
                    code += this[j, i] ? "true " : "false";
                    code += j != right - 1 ? ", " : " },\n";
                }
            }
            return code;
        }
        
    }

    
}
