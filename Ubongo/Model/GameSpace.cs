﻿using System.Collections.Generic;
using System.Linq;

namespace Ubongo.Model
{
    public class GameSpace
    {
        public GameSpace(bool[,] gameSpaceDefinition, List<Block> blocks)
        {
            GameSpaceDefinition = gameSpaceDefinition;
            Blocks = blocks;
        }

        public bool[,] GameSpaceDefinition { get; private set; }

        public bool this[int x, int y] => GameSpaceDefinition[y, x];

        public int Width => GameSpaceDefinition.GetLength(1);

        public int Height => GameSpaceDefinition.GetLength(0);

        public List<Block> Blocks { get; private set; } 

        public bool IsCorrectSolution()
        {
            var space = CopySpace();
            var blocks = Blocks.Where(block => block.HasPositionInGameSpace).ToArray();
            if (blocks.Length == 0)
                return false;

            foreach (var block in blocks)
            {
                if (!IsInSpace(block))
                    return false;

                for (int i = 0; i < block.Width; i++)
                {
                    for (int j = 0; j < block.Height; j++)
                    {
                        var isSpace = space[j + block.Y, i + block.X];
                        var isBlock = block[i, j];
                        if (!isSpace && block[i, j])
                            return false;

                        if (block[i, j])
                            space[j + block.Y, i + block.X] = false;
                    }
                }
            }

            if (space.Cast<bool>().Any(b => b))
            {
                return false;
            }
            return true;
        }

        public override string ToString()
        {
            var top = 0;
            var left = 0;
            var right = Width;
            var bottom = Height;
            var code = "{\n";

            for (int i = top; i < bottom; i++)
            {
                code += "\t{ ";
                for (int j = left; j < right; j++)
                {
                    code += this[j,i] ? "true " : "false";
                    code += j != right - 1? ", " : " },\n";
                }
            }
            code += "}";
            return code;
        }


        private bool IsInSpace(Block block)
        {
            return block.X >= 0 && block.Y >= 0 &&
                   block.X + block.Width <= Width &&
                   block.Y + block.Height <= Height;

        }

        private bool[,] CopySpace()
        {
            bool[,] space = new bool[GameSpaceDefinition.GetLength(0), GameSpaceDefinition.GetLength(1)];
            for (int i = 0; i < GameSpaceDefinition.GetLength(0); i++)
            {
                for (int j = 0; j < GameSpaceDefinition.GetLength(1); j++)
                {
                    space[i, j] = GameSpaceDefinition[i, j];
                }
            }

            return space;
        }

        
    }
}
