﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using Ubongo.ViewModel;

namespace TaskGenerator
{
    public class MainViewModel : PropertyChangedNotificator
    {
        private string code;
        private const int width = 15;
        private const int height = 15;
        private bool reset = false;

        public MainViewModel()
        {
            CmdClick = new Command<Field>(ExecuteClick);
            Reset = new Command(ExecuteReset);

            for (int i = 0; i < height; i++)
            {
                Fields[i] = new Field[width];
                for (int j = 0; j < width; j++)
                {
                    Fields[i][j] = new Field(i,j);
                }
            }

            Blocks = new BlockData[]
            {
                new BlockData(Ubongo.Model.Blocks.A, "A"),
                new BlockData(Ubongo.Model.Blocks.B, "B"),
                new BlockData(Ubongo.Model.Blocks.C, "C"),
                new BlockData(Ubongo.Model.Blocks.D, "D"),
                new BlockData(Ubongo.Model.Blocks.E, "E"),
                new BlockData(Ubongo.Model.Blocks.F, "F"),
                new BlockData(Ubongo.Model.Blocks.G, "G"),
                new BlockData(Ubongo.Model.Blocks.H, "H"),
                new BlockData(Ubongo.Model.Blocks.I, "I"),
                new BlockData(Ubongo.Model.Blocks.J, "J"),
                new BlockData(Ubongo.Model.Blocks.K, "K"),
                new BlockData(Ubongo.Model.Blocks.L, "L"),
                new BlockData(Ubongo.Model.Blocks.M, "M"),
                new BlockData(Ubongo.Model.Blocks.N, "N"),
                new BlockData(Ubongo.Model.Blocks.O, "O"),
                new BlockData(Ubongo.Model.Blocks.P, "P"),
                new BlockData(Ubongo.Model.Blocks.Q, "Q"),
                new BlockData(Ubongo.Model.Blocks.R, "R"),
            };
            foreach (var blockData in Blocks)
            {
                blockData.PropertyChanged += BlockDataOnPropertyChanged;
            }

        }

        private void BlockDataOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if(!reset)
                Code = GenerateCode();
        }

        public Command<Field> CmdClick { get; }

        public Command Reset { get; }

        public Field[][] Fields { get; } = new Field[height][];

        public BlockData[] Blocks { get; } 

        public string Code
        {
            get { return code; }
            set
            {
                code = value; 
                OnPropertyChanged(nameof(Code));
            }
        }

        private void ExecuteClick(Field field)
        {
            field.Used = !field.Used;
            Code = GenerateCode();
        }

        private void ExecuteReset()
        {
            reset = true;
            foreach (var blockData in Blocks)
            {
                blockData.Count = 0;
            }
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    Fields[i][j].Used = false;
                }
            }

            reset = false;
            Code = "";
        }

        private string GenerateCode()
        {
            var top = GetTop();
            var left = GetLeft();
            var right = GetRight();
            var bottom = GetBottom();
            var code = "new GameSpace(new bool[,]\n{\n";

            for (int i = top; i <= bottom; i++)
            {
                code += "\t{ ";
                var comment = "\t// ";
                for (int j = left; j <= right; j++)
                {
                    comment += Fields[i][j].Used ? "██" : "  ";
                    code += Fields[i][j].Used ? "true " : "false";
                    code += j != right ? ", " : " },"+ comment +"\n";
                }
            }
            code += "},\nnew List<Block>\n{"+ GenerateBlocksCode() +"}),";
            return code;
        }

        private string GenerateBlocksCode()
        {
            string c = "\n";
            foreach (var blockData in Blocks)
            {
                for (int i = 0; i < blockData.Count; i++)
                {
                    c += $"\tBlocks.{blockData.BlockName},\n";
                }
            }

            return c;
        }

        private int GetLeft()
        {
            var left = int.MaxValue;
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (Fields[i][j].Used)
                    {
                        if (j < left)
                            left = j;
                        break;
                    }
                }
            }
            return left;
        }

        private int GetTop()
        {
            var top = int.MaxValue;
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    if (Fields[j][i].Used)
                    {
                        if (j < top)
                            top = j;
                        break;
                    }
                }
            }
            return top;
        }

        private int GetRight()
        {
            var right = int.MinValue;
            for (int i = 0; i < height; i++)
            {
                for (int j = width - 1; j >= 0; j--)
                {
                    if (Fields[i][j].Used)
                    {
                        if (j > right)
                            right = j;
                        break;
                    }
                }
            }
            return right;
        }

        private int GetBottom()
        {
            var bottom = int.MinValue;
            for (int i = 0; i < width; i++)
            {
                for (int j = height - 1; j >= 0; j--)
                {
                    if (Fields[j][i].Used)
                    {
                        if (j > bottom)
                            bottom = j;
                        break;
                    }
                }
            }
            return bottom;
        }
    }

   

    public class Field : PropertyChangedNotificator
    {
        private bool used = false;

        public Field(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int X { get; set; }

        public int Y { get; set; }

        public bool Used
        {
            get { return used; }
            set
            {
                used = value;
                OnPropertyChanged(nameof(Used));
            }
        }

        public override string ToString()
        {
            return $"[{X},{Y}]";
        }
    }
}
