﻿using System;
using Ubongo.Model;
using Ubongo.View;
using Ubongo.ViewModel;

namespace TaskGenerator
{
    public class BlockData : PropertyChangedNotificator
    {
        private int count = 0;

        public BlockData(Block block, string blockName)
        {
            BlockName = blockName;
            BlockPolygon = new BlockPolygon(block, 20);
            Inc = new Command(() => Count++ );
            Dec = new Command((() => Count = Math.Max(0, Count - 1)));
        }

        public BlockPolygon BlockPolygon { get; }

        public string BlockName { get; }

        public int Count
        {
            get { return count; }
            set
            {
                count = value;
                OnPropertyChanged(nameof(Count));
            }
        }

        public Command Inc { get; }
        public Command Dec { get; }

    }
}
