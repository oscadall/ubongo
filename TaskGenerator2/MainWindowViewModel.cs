﻿using System.Linq;
using System.Windows.Input;
using Ubongo.View;
using Ubongo.ViewModel;

namespace TaskGenerator2
{
    public class MainWindowViewModel : PropertyChangedNotificator
    {
        public MainWindowViewModel()
        {
            AddBlock = new Command<string>(ExecuteAddBlock);
            RemoveBlock = new Command<string>(ExecuteRemoveBlock);
            Generate  = new Command(ExecuteGenerate);
            Reset = new Command(ExecuteReset);

            Blocks  = new BlockPolygonData[]
            {
                new BlockPolygonData(Ubongo.Model.Blocks.A, ExecuteAddBlock, ExecuteRemoveBlock),
                new BlockPolygonData(Ubongo.Model.Blocks.B, ExecuteAddBlock, ExecuteRemoveBlock),
                new BlockPolygonData(Ubongo.Model.Blocks.C, ExecuteAddBlock, ExecuteRemoveBlock),
                new BlockPolygonData(Ubongo.Model.Blocks.D, ExecuteAddBlock, ExecuteRemoveBlock),
                new BlockPolygonData(Ubongo.Model.Blocks.E, ExecuteAddBlock, ExecuteRemoveBlock),
                new BlockPolygonData(Ubongo.Model.Blocks.F, ExecuteAddBlock, ExecuteRemoveBlock),
                new BlockPolygonData(Ubongo.Model.Blocks.G, ExecuteAddBlock, ExecuteRemoveBlock),
                new BlockPolygonData(Ubongo.Model.Blocks.H, ExecuteAddBlock, ExecuteRemoveBlock),
                new BlockPolygonData(Ubongo.Model.Blocks.I, ExecuteAddBlock, ExecuteRemoveBlock),
                new BlockPolygonData(Ubongo.Model.Blocks.J, ExecuteAddBlock, ExecuteRemoveBlock),
                new BlockPolygonData(Ubongo.Model.Blocks.K, ExecuteAddBlock, ExecuteRemoveBlock),
                new BlockPolygonData(Ubongo.Model.Blocks.L, ExecuteAddBlock, ExecuteRemoveBlock),
                new BlockPolygonData(Ubongo.Model.Blocks.M, ExecuteAddBlock, ExecuteRemoveBlock),
                new BlockPolygonData(Ubongo.Model.Blocks.N, ExecuteAddBlock, ExecuteRemoveBlock),
                new BlockPolygonData(Ubongo.Model.Blocks.O, ExecuteAddBlock, ExecuteRemoveBlock),
                new BlockPolygonData(Ubongo.Model.Blocks.P, ExecuteAddBlock, ExecuteRemoveBlock),
                new BlockPolygonData(Ubongo.Model.Blocks.Q, ExecuteAddBlock, ExecuteRemoveBlock),
                new BlockPolygonData(Ubongo.Model.Blocks.R, ExecuteAddBlock, ExecuteRemoveBlock),
                new BlockPolygonData(Ubongo.Model.Blocks.S, ExecuteAddBlock, ExecuteRemoveBlock),
            };
        }

        public int Count
        {
            get { return count; }
            set
            {
                count = value;
                OnPropertyChanged(nameof(Count));
            }
        }

        public string Code { get; set; }

        public BlockPolygonData[] Blocks { get; }

        public ExtendedGameSpaceControl Control;
        private int count;

        public ICommand AddBlock { get; }

        public ICommand RemoveBlock { get; }

        public ICommand Generate { get; }

        public ICommand Reset { get; }

        private void ExecuteGenerate()
        {
            Code = Control.GenerateCode();
            OnPropertyChanged(nameof(Code));
        }


        
        private void ExecuteAddBlock(string blockName)
        {
            Count++;
            Control.AddRemovableBlockPolygon(Ubongo.Model.Blocks.NewBlock(blockName));
        }

        private void ExecuteRemoveBlock(string blockName)
        {
            Count--;
            var b = Control.Children.OfType<BlockPolygon>().First(bp => bp.Block.Name == blockName);
            Control.Children.Remove(b);
            Control.BlockPolygons.Remove(b);
        }

        private void ExecuteReset()
        {
            //var blocks = Control.Children.OfType<BlockPolygon>().ToArray();
            //foreach (var bp in blocks)
            //{
            //    Control.Children.Remove(bp);
            //    Control.BlockPolygons.Remove(bp);
            //}
            foreach (var block in Blocks)
            {
                block.Reset();
            }
            Code = "";
            OnPropertyChanged(nameof(Code));
        }
    }
}
