﻿using System;
using Ubongo.Model;
using Ubongo.View;
using Ubongo.ViewModel;

namespace TaskGenerator2
{
    public class BlockPolygonData : PropertyChangedNotificator
    {
        private int count = 0;
        private readonly Action<string> addAction;
        private readonly Action<string> removeAction;

        public BlockPolygonData(Block block, Action<string> addAction, Action<string> removeAction)
        {
            BlockPolygon = new BlockPolygon(block, 20);
            this.addAction = addAction;
            this.removeAction = removeAction;
            Inc = new Command<string>(ExecuteAdd);
            Dec = new Command<string>(ExecuteRemove);
        }

        public BlockPolygon BlockPolygon { get; }


        public int Count
        {
            get { return count; }
            set
            {
                count = value;
                OnPropertyChanged(nameof(Count));
            }
        }

        public Command<string> Inc { get; }
        public Command<string> Dec { get; }

        public void Reset()
        {
            while (Count != 0)
            {
                ExecuteRemove(BlockPolygon.Block.Name);
            }
        }

        private void ExecuteAdd(string name)
        {
            Count++;
            addAction(name);
        }

        private void ExecuteRemove(string name)
        {
            if (Count > 0)
            {
                Count--;
                removeAction(name);
            }
            
        }

        

    }
}
