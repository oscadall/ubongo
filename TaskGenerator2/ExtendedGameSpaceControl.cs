﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ubongo.Model;
using Ubongo.View;
using Ubongo.ViewModel;

namespace TaskGenerator2
{
    public class ExtendedGameSpaceControl : GameSpaceControl
    {
        private const int width = 20;
        private const int height = 20;
        public ExtendedGameSpaceControl()
        {
            bool [,] place = new bool[height,width];
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    place[i, j] = true;
                }
            }
            
            GameSpace = new GameSpace(place, new List<Block>());
            EdgeSize = 30;
        }

        

        public void AddRemovableBlockPolygon(Block block)
        {
            var polygon = new BlockPolygon(block, EdgeSize);
            polygon.GotMouseCapture += GameSpaceControl_GotMouseCapture;
            polygon.LostMouseCapture += Polygon_LostMouseCapture;
            BlockPolygons.Add(polygon);
            Children.Add(polygon);
            SetLeft(polygon,0);
            SetTop(polygon, 0);
        }

        
        public string GenerateCode()
        {
            var fields = new bool[height, width];
            var blockNames = new List<string>();
            var blocks = BlockPolygons.Select(b => b.Block);

            foreach (var block in blocks)
            {
                blockNames.Add(block.Name);
                for (int i = 0; i < block.Width; i++)
                {
                    for (int j = 0; j < block.Height; j++)
                    {
                        if (fields[j + block.Y, i + block.X] && block.BlockDefinition[j,i])
                            return "ERROR";

                        if (block.BlockDefinition[j,i])
                            fields[j + block.Y, i + block.X] = true;
                    }
                }
            }

            var top = GetTop(fields);
            var left = GetLeft(fields);
            var right = GetRight(fields);
            var bottom = GetBottom(fields);

            var code = "// \nnew GameSpace(new[,]\n{\n";

            for (int i = top; i <= bottom; i++)
            {
                code += "\t{ ";
                var comment = "\t// ";
                for (int j = left; j <= right; j++)
                {
                    comment += fields[i,j] ? "██" : "  ";
                    code += fields[i,j] ? "true " : "false";
                    code += j != right ? ", " : " }," + comment + "\n";
                }
            }
            code += "},\nnew List<Block>\n{" + GenerateBlocksCode(blockNames) + "}),";
            return code;
        }

        private string GenerateBlocksCode(List<string> names)
        {
            string c = "\n";
            foreach (var name in names)
            {
                c += $"\tBlocks.{name},\n";
            }
            return c;
        }

        private int GetLeft(bool[,] fields)
        {
            var left = int.MaxValue;
            for (int h = 0; h < height; h++)
            {
                for (int w = 0; w < width; w++)
                {
                    if (fields[h, w])
                    {
                        if (w < left)
                            left = w;
                        break;
                    }
                }
            }
            return left;
        }

        private int GetTop(bool[,] fields)
        {
            var top = int.MaxValue;
            for (int w = 0; w < width; w++)
            {
                for (int h = 0; h < height; h++)
                {
                    if (fields[h,w])
                    {
                        if (h < top)
                            top = h;
                        break;
                    }
                }
            }
            return top;
        }

        private int GetRight(bool[,] fields)
        {
            var right = int.MinValue;
            for (int h = 0; h < height; h++)
            {
                for (int w = width - 1; w >= 0; w--)
                {
                    if (fields[h, w])
                    {
                        //if (w > right)
                        //    right = w;
                        right = Math.Max(right, w);
                        break;
                    }
                }
            }
            return right;
        }

        private int GetBottom(bool[,] fields)
        {
            var bottom = int.MinValue;
            for (int w = 0; w < width; w++)
            {
                for (int h = height - 1; h >= 0; h--)
                {
                    if (fields[h, w])
                    {
                        //if (h > bottom)
                        //    bottom = h;
                        bottom = Math.Max(bottom, h);
                        break;
                    }
                }
            }
            return bottom;
        }

       
    }



  

}

